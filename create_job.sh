#!/bin/bash

# ===========================================================================================================
# title         : create_job.sh
# usage         : ./create_job.sh $OC_JOB_ID $BUILD_ENV_DOCKER_IMAGE $INSTALLATION_ROOT
#                 $SPACK_ENV $COMMIT_SHA $RELEASE_NAME $LAB_KERNEL_ROOT
# description   : creates OKD job yaml file that builds/updates spack environment and creates Lab kernel
# ===========================================================================================================

OC_JOB_ID=$1
BUILD_ENV_DOCKER_IMAGE=$2
INSTALLATION_ROOT=$3
EBRAINS_SPACK_ENV=$4
COMMIT_SHA=$5
RELEASE_NAME=$6
LAB_KERNEL_ROOT=$7
UPDATE_SPACK_OCI_CACHES=$8
OCI_CACHE_PREFIX=$9

cat <<EOT >> simplejob.yml
apiVersion: batch/v1
kind: Job
metadata:
  name: simplejob${OC_JOB_ID}
spec:
  parallelism: 1
  completions: 1
  backoffLimit: 0
  template:
    metadata:
      name: testjob
    spec:
      containers:
      - name: simplejob
        image: ${BUILD_ENV_DOCKER_IMAGE}
        imagePullPolicy: Always
        resources:
          limits:
            cpu: '8'
            memory: '32Gi'
          requests:
            cpu: '4'
            memory: '20Gi'
        volumeMounts:
          - name: sharedbin
            mountPath: /srv
          - name: tmp
            mountPath: /tmp
        command:
        - /bin/bash
        - -c
        - |
          # create root dir if it doesn't exist
          mkdir -p \$INSTALLATION_ROOT
          
          # reset spack repository dir by cloning the selected version
          if [ ! -d \$EBRAINS_REPO_PATH ]; then git clone ${CI_PROJECT_URL} --recurse-submodules \$EBRAINS_REPO_PATH; fi
          cd \$EBRAINS_REPO_PATH
          git fetch origin
          git reset --hard \$COMMIT_SHA
          git submodule update --force

          # reset build error log dir (delete previous logs to save space)
          rm -rf \$BUILD_LOGS_DIR
          mkdir -p \$BUILD_LOGS_DIR
          cd \$BUILD_LOGS_DIR
          
          # run installation script
          bash \$EBRAINS_REPO_PATH/install_spack_env.sh \$SPACK_JOBS \$INSTALLATION_ROOT \$EBRAINS_REPO_PATH \$EBRAINS_SPACK_ENV "" \$UPDATE_SPACK_OCI_CACHES \$OCI_CACHE_PREFIX
          
          if [ \$? -eq 0 ]
          then
            # build process succeeded - create or update kernel on the NFS based on the current spack environment
            bash \$EBRAINS_REPO_PATH/create_JupyterLab_kernel.sh \$INSTALLATION_ROOT \$EBRAINS_SPACK_ENV \$RELEASE_NAME \$LAB_KERNEL_ROOT && exit 0
          else
            # build process failed - keep spack build logs and fail the pipeline
            cp -r /tmp/spack/spack-stage/* \$BUILD_LOGS_DIR
            exit 1
          fi
        env:
          - name: SYSTEMNAME
            value: ebrainslab
          - name: INSTALLATION_ROOT
            value: $INSTALLATION_ROOT
          - name: EBRAINS_SPACK_ENV
            value: $EBRAINS_SPACK_ENV
          - name: UPDATE_SPACK_OCI_CACHES
            value: '$UPDATE_SPACK_OCI_CACHES'
          - name: OCI_CACHE_PREFIX
            value: $OCI_CACHE_PREFIX
          - name: COMMIT_SHA
            value: $COMMIT_SHA
          - name: RELEASE_NAME
            value: $RELEASE_NAME
          - name: LAB_KERNEL_ROOT
            value: $LAB_KERNEL_ROOT
          - name: BUILD_LOGS_DIR
            value: /srv/build_logs/$EBRAINS_SPACK_ENV
          - name: EBRAINS_REPO_PATH
            value: $INSTALLATION_ROOT/ebrains-spack-builds
          - name: SPACK_JOBS
            value: '6'
      volumes:
        - name: sharedbin
          persistentVolumeClaim:
            claimName: shared-binaries
        - name: tmp
          emptyDir: {}
      restartPolicy: Never
EOT
