# ebrains-spack-builds

Build and distribute EBRAINS software tools with Spack.

## Introduction

Building and deploying EBRAINS tools to make them available for direct use in the Collaboratory Lab environment is a challenging task, mainly due to the number of tools and the increasing complexity of resolving their dependency conflicts. To efficiently solve this problem, we have implemented a delivery strategy based on [Spack](https://spack.io/), a package manager specifically designed to simplify the installation and management of scientific software (more information [here](https://drive.ebrains.eu/f/73bcfe5acf924cef83cc/)).

Spack is highly customizable and flexible, allowing developers to create their own packages and specify their dependencies and custom build configurations. It supports a wide range of programming languages (Python, R, C, C++, Fortran etc), different hardware architectures and operating systems. All an EBRAINS developer needs to do is create a Spack package (simple description of the build logic of their software). Then Spack automatically handles downloading, building, and installing the entire EBRAINS environment, managing all their dependencies and other requirements.

## Repository

This repository is where the EBRAINS Spack packages are maintained. It contains:
- the EBRAINS [Spack repository](https://spack.readthedocs.io/en/latest/repositories.html), that contains all available EBRAINS Spack packages ([packages/](packages))
- the latest EBRAINS [Spack environment](https://spack.readthedocs.io/en/latest/environments.html) ([spack.yaml](spack.yaml)), a list of the specific versions of EBRAINS tools that are part of the software release
- a script ([install_spack_env.sh](install_spack_env.sh)) that can be used to install the latest spack environment on any machine (either install from scratch or update an existing installation)
- CI/CD pipelines configured for (a) testing the installation of new packages and (b) deploying the environment to the EBRAINS Collaboratory Lab.
- (WIP) all the site-specific configurations that have been used to install the EBRAINS Spack environment in different systems, such as the EBRAINS Lab and the Fenix HPC systems.

## Contributing

HBP's developers/component owners that want their EBRAINS tools to be included in the official EBRAINS releases and deployed for direct use from the Lab environment should follow those steps:

- Fork this repository
- Clone the forked repository to local machine (or use the Gitlab Web IDE)
- Create a new Spack package for your tool. For instructions on how to do this, see [the Spack documentation](https://spack.readthedocs.io/en/latest/packaging_guide.html) and the developer's section of [our documentation](https://drive.ebrains.eu/f/73bcfe5acf924cef83cc/). **Add the new package** to the repository, by creating a directory inside [packages/](./packages/) and copying the package recipe there. Also don't forget to **add a spec** to [spack.yaml](spack.yaml)
- Commit the new package and push to a new branch, e.g. `add-packagename`
- Create a Merge Request to merge your changes into the master branch of the original repository
- Check the results of the automated test build job and solve any issues
- Once the tests pass, wait for TC to approve and merge. Once merged, the package will be automatically built and deployed to the integration environment of the Collaboratory Lab (lab-int).

## Installing the EBRAINS official Software release

To install the EBRAINS Spack environment on a new machine, it is recommended to follow exactly the steps from [install_spack_env.sh](install_spack_env.sh), that installs or updates and tests the spack environment defined in this repository. In general, you need to:

Clone this repository. You can use the `ebrains-yy-mm` branches to install the EBRAINS official releases, the `experimental-rel` branch for the experimental release, or the `master` branch for the latest state:
```
git clone --branch {branch-name} https://gitlab.ebrains.eu/ri/tech-hub/platform/esd/ebrains-spack-builds.git
```

Clone Spack. We currently use version v0.23.1:
```
git clone --depth 1 -c advice.detachedHead=false -c feature.manyFiles=true --branch v0.23.1 https://github.com/spack/spack
```

Activate Spack:
```
. spack/share/spack/setup-env.sh
```

Add the project repository to your Spack environment:
```
spack repo add ebrains-spack-builds
```

Define your site-specific configurations:
```
export SYSTEMNAME=<your-system-name>
mkdir ebrains-spack-builds/site-config/$SYSTEMNAME
# copy any site-specific .yaml files inside the new dir
```

Activate the environment and install:
```
spack env activate ebrains-spack-builds
spack install --fresh
```

## Troubleshooting

If you encounter any issues with installing, testing, or using the EBRAINS Spack environment or a specific package, please [open an issue](https://gitlab.ebrains.eu/ri/tech-hub/platform/esd/ebrains-spack-builds/-/issues/new) using the provided templates, or contact the Technical Coordination team directly.
