# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyNmodlGlia(PythonPackage):
    """
    Patch to use NMODL within the BSB
    """

    homepage = "https://github.com/dbbs-lab/glia"
    pypi = "nmodl-glia/nmodl_glia-4.0.1.tar.gz"

    license("GPL-3.0-only")
    maintainers = ["helveg","filimarc","drodarie"]

    version("4.0.1", sha256="c3b3dad203eac1f394d6a4ca6e4f42d25d5eebc013970309f1453c7ca3e5c5a3")

    depends_on("py-flit-core@3.2:4.0", type="build")
    depends_on("py-numpy@1.21:")
    depends_on("py-errr@1.2:")
    depends_on("py-nmodl@0.5:")
