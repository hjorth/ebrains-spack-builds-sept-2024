# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyPyswarms(PythonPackage):

    homepage = "https://github.com/ljvmiranda921/pyswarms"
    pypi = "pyswarms/pyswarms-1.3.0.tar.gz"

    maintainers = ["hjorth", "elmath"]

    version("1.3.0", sha256="1204aa9c332c662113e3c37d1b109906f4a0859b29ded80c1582dc66387ce34b") 

    depends_on("py-setuptools",         type=("build"))
    depends_on("py-scipy",              type=("build", "run"))
    depends_on("py-numpy",              type=("build", "run"))
    depends_on("py-matplotlib@1.3.1:",  type=("build", "run"))
    depends_on("py-attrs",              type=("build", "run"))
    depends_on("py-tqdm",               type=("build", "run"))
    depends_on("py-future",             type=("build", "run"))
    depends_on("py-pyyaml",             type=("build", "run"))
