# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyJunitparser(PythonPackage):
    """
    junitparser handles JUnit/xUnit Result XML files. Use it to parse and
    manipulate existing Result XML files, or create new JUnit/xUnit result XMLs
    from scratch.
    """

    homepage = "https://github.com/weiwei/junitparser"
    #url      = "https://files.pythonhosted.org/packages/py2.py3/j/junitparser/junitparser-2.8.0-py2.py3-none-any.whl"
    url      = "https://github.com/weiwei/junitparser/archive/refs/tags/2.8.0.tar.gz"

    maintainers = ['terhorstd']

    version('2.8.0', sha256='94f7e0e45a1e6d729e35f29f28d06d5d314f17bdf68c9a4b1abc20eff0a6cca5')

    # Only add the python dependency if you need specific versions. A generic
    # python dependency is added implicity by the PythonPackage class.
    # depends_on('python@2.X:2.Y,3.Z:', type=('build', 'run'))
    depends_on('py-setuptools@44.0:', type='build')
    depends_on('py-wheel@0.37:',      type=('build', 'run'))
    depends_on('py-future',           type=('build', 'run'))

