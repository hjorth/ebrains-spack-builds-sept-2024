# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)


from spack.package import *


class PyMpipool(PythonPackage):
    """mpipool offers MPI based parallel execution of tasks through implementations of Python's standard library interfaces"""

    homepage = "https://github.com/mpipool/mpipool"
    pypi = "mpipool/mpipool-2.2.1.tar.gz"

    version("2.2.1", sha256="dc735b994349ae3e06fce7c3601523ba062125ffa6dd4c6c51a94c168c9ff92c")

    maintainers=["helveg"]

    depends_on("py-flit-core@3.2:4", type="build")
    depends_on("py-mpi4py@3.0.3:")
    depends_on("py-errr@1.0:")
    depends_on("py-tblib@1.7.0:")
    depends_on("py-dill@0.3.3:")
