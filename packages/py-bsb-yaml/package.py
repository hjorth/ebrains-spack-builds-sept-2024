# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyBsbYaml(PythonPackage):
    """A plugin that allows the user to write their models' configuration in the yaml format, for the BSB framework."""

    homepage = "https://github.com/dbbs-lab/bsb-yaml"
    pypi = "bsb-yaml/bsb_yaml-4.2.2.tar.gz"

    license("GPL-3.0-only")
    maintainers = ["helveg","filimarc","drodarie"]

    version("4.2.2", sha256="c5614bc5fe57b78a445303756819a8d4ba032924484f88a07f6c26dd7e5afbec")

    depends_on("py-flit-core@3.2:4.0", type="build")
    depends_on("py-bsb-core@5.0.0:")
    depends_on("py-pyyaml@6.0:")
    depends_on("py-shortuuid")
