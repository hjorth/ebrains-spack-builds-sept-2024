# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyNestml(PythonPackage):
    """A Python package for writing and generating code for (spiking) neuron
    models and synaptic plasticity rules. Supports C++ code generation for
    NEST Simulator as well as other target platforms.
    """

    homepage = 'https://nestml.readthedocs.org/'
    url      = 'https://pypi.org/packages/py3/N/NESTML/NESTML-8.0.0-py3-none-any.whl'
    git      = 'https://github.com/nest/nestml/'

    maintainers = ['clinssen', 'pnbabu']

    version('8.0.0', sha256='bb2182fadd5f3ff7fa538e7f9865bafefb8be67938c83028174491768e88ef09', expand=False)
    version('7.0.2', sha256='3611239ff8436bf1c74b878562564007285c6da5df0317c6cd52f65e6bcd3f8b', expand=False)
    version('7.0.1', sha256='38392bdd06c5be5af65050153f34fb78dee6032158b268b83599bd70aab5c030', expand=False)
    version('7.0.0', sha256='4e271048b4a9ad2c161dda98d3ee25e143537649a264d521deb0ff5543020d73', expand=False)
    version('6.0.0', sha256='224993f175b9599a3662e65afdaf57e355246c0eab5f88b795fdb089ea28b39e', expand=False)
    version('5.3.0', sha256='8de543d7d3a166cd4d6a0d536a2d4e769513bbf8d7aeaf64458e9d9c21fe546e', expand=False)
    version('5.2.0', sha256='acb703bf9c7f70304bd5d547dccd6a6a219f8acb298a6412df779b808241eb14', expand=False)

    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('py-pip', type='build')
    depends_on('py-numpy@1.8.2:', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-sympy@1.1.1:1.10.1', type=('build', 'run'))
    depends_on('py-antlr4-python3-runtime@4.10:', type=('build', 'run'))
    depends_on('py-setuptools', type=('build', 'run'))
    depends_on('py-jinja2@2.10:', type=('build', 'run'))
    depends_on('py-astropy', type=('build', 'run'))
    depends_on('py-odetoolbox@2.4:', type=('build', 'run'))
    depends_on('nest', type=('build', 'run'))
    depends_on('py-pytest', type='test')
