# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyFlaskRestx(PythonPackage):
    """
    Requirement necessary for py-tvb-framework package.
    """

    homepage = "https://pypi.org/project/flask-restx"
    pypi = 'flask-restx/flask-restx-1.3.0.tar.gz'

    maintainers = ['paulapopa', 'ldomide']

    version('1.3.0', '4f3d3fa7b6191fcc715b18c201a12cd875176f92ba4acc61626ccfd571ee1728')
    version('1.0.5', 'e23dc4ff24869c92faa719b7a58be1203ed741275ff32c9f03d0ab56ed01546c')

    # python_requires
    depends_on('python@3.8:', type=('build', 'run'))

    # setup_requires
    depends_on('py-pip', type=('build'))
    depends_on('py-setuptools', type=('build'))
    depends_on('py-flask', type=('build', 'run'))
    depends_on('py-aniso8601', type=('build', 'run'))
    depends_on('py-jsonschema', type=('build', 'run'))
    depends_on('py-pytz', type=('build', 'run'))
    depends_on('py-werkzeug', type=('build', 'run'))

