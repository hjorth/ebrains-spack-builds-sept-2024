# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyEbrainsValidationClient(PythonPackage):
    """Python client for the EBRAINS Validation Framework web services"""

    homepage = "https://ebrains-validation-client.readthedocs.io/"
    pypi     = "ebrains_validation_framework/ebrains_validation_framework-0.9.1.tar.gz"
    git      = "https://github.com/HumanBrainProject/ebrains-validation-client"

    maintainers = ["apdavison"]

    version("0.9.1", sha256="388b6b758fd96dc6a6a49616741ef19b37883422af134304c9d7f806eb0d8993")

    depends_on('python@3.8:')
    depends_on('py-setuptools',            type=('build'))
    depends_on('py-requests@2.26.0:',      type=('build', 'run'))
    depends_on('py-nameparser@1.1.1:',     type=('build', 'run'))
    depends_on('py-ebrains-drive@0.6.0:',  type=('build', 'run'))
