# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class RRgsl(RPackage):
    """This is an interface package to solve ODE systems using gsl_odeiv2 within R. The system to solve has to be defined using C code: a shared library, dynamically loaded by this package when solving."""

    homepage = "https://github.com/icpm-kth/rgsl"
    url = "https://github.com/icpm-kth/rgsl/archive/refs/tags/v0.1.tar.gz"

    maintainers = ["akramer", "oliviaeriksson"]

    version("0.1.1", sha256="a2d8fdbbb6c4f9937d6abaa56d912418b42931ae67067357e9bf5a032cc1a833")
    version("0.1",   sha256="dd6a14dda8731f4cf32149c8a9842e4e2ba424c8404f4d6839108153b20e4c3e")

    # GNU Scientific Library: https://www.gnu.org/software/gsl
    # spack: https://spack.readthedocs.io/en/latest/package_list.html#gsl
    depends_on("gsl")
    depends_on("pkgconfig")
