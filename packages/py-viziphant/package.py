# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyViziphant(PythonPackage):
    """Viziphant is a package for the visualization of the analysis results of electrophysiology data in Python"""

    homepage = "https://viziphant.readthedocs.io/en/latest/"
    pypi = "viziphant/viziphant-0.1.0.tar.gz"

    maintainers = ['moritzkern']

    version('0.4.0', sha256='ae8c3df517d7781c184086909fad95f30c093534cfb35b74eadf330fa144e336')
    version('0.3.0', sha256='40d2970b8384d8e1c5d324caf1b1188391d7e651375ce08dd518bcf4ac6477f2')
    version('0.2.0', sha256='044b5c92de169dfafd9665efe2c310e917d2c21980bcc9f560d5c727161f9bd8')
    version('0.1.0', sha256='8fd56ec8633f799396dc33fbace95d2553bedb17f680a8c0e97f43b3a629bf6c')

    depends_on('py-setuptools', type='build')
    depends_on('python@3.7:3.11', type=('build', 'run'))
    depends_on('py-neo@0.9.0:', type=('build', 'run'))
    depends_on('py-elephant@0.9.0:', type=('build', 'run'))
    depends_on('py-numpy@1.18.1:', type=('build', 'run'))
    depends_on('py-quantities@0.12.1:', type=('build', 'run'))
    depends_on('py-six@1.10.0:', type=('build', 'run'))
    depends_on('py-matplotlib@3.3.2:', type=('build', 'run'))
    depends_on('py-seaborn@0.9.0:', type=('build', 'run'))
    depends_on('py-bokeh@2.4.3:', type=('build', 'run'), when="@0.4.0:")
    depends_on('py-holoviews@1.16.0:', type=('build', 'run'), when="@0.4.0:")
    depends_on('py-networkx@2.8.6:', type=('build', 'run'), when="@0.4.0:")

    depends_on("py-pytest", type=("test"), when="@0.3.0:")
    depends_on("py-elephant@0.9.0:+extras", type=("test"), when="@0.3.0:")

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        # run tests
        # TODO: replace if clause with @run_after('install', when='@0.3.0:') after update to Spack v0.19
        if self.spec.version >= Version('0.3.0'):
            pytest = which('pytest')
            pytest()
