# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class Psmisc(AutotoolsPackage):
    """A package of small utilities that use the proc file-system."""

    homepage = "http://psmisc.sf.net/"
    url      = "https://sourceforge.net/projects/psmisc/files/psmisc/psmisc-23.6.tar.xz"

    version('23.6', sha256='257dde06159a4c49223d06f1cccbeb68933a4514fc8f1d77c64b54f0d108822a')
    version('23.5', sha256='dc37ecc2f7e4a90a94956accc6e1c77adb71316b7c9cbd39b26738db0c3ae58b')
    version('23.4', sha256='7f0cceeace2050c525f3ebb35f3ba01d618b8d690620580bdb8cd8269a0c1679')
    version('23.3', sha256='41750e1a5abf7ed2647b094f58127c73dbce6876f77ba4e0a7e0995ae5c7279a')

    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool', type='build')
    depends_on('gettext', type=('build', 'link'))
    depends_on('ncurses', type=('build', 'link'))
    depends_on('dejagnu', type=('build', 'link'))
