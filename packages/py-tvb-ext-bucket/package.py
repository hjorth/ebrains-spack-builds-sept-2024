from spack.package import *


class PyTvbExtBucket(PythonPackage):
    """
    Jupyter Lab extension tvb-ext-bucket for accessing EBRAINS data proxy through a GUI
    """

    homepage = "https://www.thevirtualbrain.org/"
    pypi = "tvb-ext-bucket/tvb_ext_bucket-1.0.0.tar.gz"
    maintainers = ['ldomide', 'adrianciu']

    version("1.0.0", sha256="43c7e75129d65b14ef347d9ce2de4d152b308d5464e015e3577f480a7d75bbee")

    depends_on('py-setuptools', type='build')
    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('py-jupyter-server', type=('build', 'run'))
    depends_on('py-ebrains-drive@0.5.0:', type=('build', 'run'))
    depends_on('py-hatchling@1.5.0:', type='build')
    depends_on('py-jupyterlab@3.4.7:3', type=('build', 'run'))
    depends_on('py-hatch-nodejs-version@0.3.1:', type='build')
    depends_on('npm', type='build')
    depends_on('node-js', type='build')
    depends_on('py-hatch-jupyter-builder', type='build')

    depends_on('py-pytest', type='test')
    depends_on('py-pytest-asyncio', type='test')
    depends_on('py-pytest-mock', type='test')
    depends_on('py-pytest-tornasync', type='test')

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which('pytest')
        pytest()
