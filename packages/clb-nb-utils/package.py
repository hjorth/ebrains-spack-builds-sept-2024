# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *

# Usage
# from clb_nb_utils import oauth
# oauth.get_token()
class ClbNbUtils(PythonPackage):
    url      = 'https://github.com/HumanBrainProject/clb-nb-utils/archive/refs/heads/master.zip'
    maintainers = ['akarmas']
    version('0.1.0','aa079ed0a8c4806db4657a6e7b534f4d')
    depends_on('python@3.8:', type=('build','run'))
    depends_on('py-setuptools', type=('build','run'))
    depends_on('py-requests', type=('build','run'))
