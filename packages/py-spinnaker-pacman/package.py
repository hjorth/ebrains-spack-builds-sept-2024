# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PySpinnakerPacman(PythonPackage):
    """This package provides utilities for partitioning, placing a routing on a
    SpiNNaker machine."""

    homepage = "https://github.com/SpiNNakerManchester/PACMAN"
    pypi = "SpiNNaker_PACMAN/SpiNNaker_PACMAN-1!7.0.0.tar.gz"

    def url_for_version(self, version):
        name = "spinnaker_pacman" if version >= Version("7.2.0") else "SpiNNaker_PACMAN"
        url = "https://pypi.org/packages/source/s/SpiNNaker_PACMAN/{}-1!{}.tar.gz"
        return url.format(name, version)

    version("7.3.0", sha256="ef597e14aac9877c676181082e11e77ea3d4b0dfb5977b0d3ce78020229fb055")
    version("7.0.0", sha256="d9e7e620d02fda88f57a8cf157cc9421b5606d453230847f3d35985eae4c074d")

    depends_on("python@3.8:", type=("build", "run"), when="@7.3.0:")
    depends_on("python@3.7:", type=("build", "run"), when="@7.0.0:")

    depends_on("py-spinnutilities@7.3.0", type=("build", "run"), when="@7.3.0")
    depends_on("py-spinnmachine@7.3.0", type=("build", "run"), when="@7.3.0")

    depends_on("py-spinnutilities@7.0.0", type=("build", "run"), when="@7.0.0")
    depends_on("py-spinnmachine@7.0.0", type=("build", "run"), when="@7.0.0")

    depends_on("py-jsonschema", type=("build", "run"))
