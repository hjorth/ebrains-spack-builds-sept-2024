# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyPyknos(PythonPackage):
    """Python package for conditional density estimation. It either wraps or implements diverse conditional density
    estimators."""

    homepage = "https://github.com/sbi-dev/pyknos"
    pypi = "pyknos/pyknos-0.16.0.tar.gz"

    version("0.16.0", "4e1db834d8a5fd847882a081937732fea6798668b72293ae052765e7bfc371c3")

    depends_on("python@3.8:", type=("build", "run"))
    depends_on("py-setuptools", type="build")
    depends_on("py-matplotlib", type=("build", "run"))
    depends_on("py-nflows@0.14", type=("build", "run"))
    depends_on("py-numpy", type=("build", "run"))
    depends_on("py-tensorboard", type=("build", "run"))
    depends_on("py-torch", type=("build", "run"))
    depends_on("py-tqdm", type=("build", "run"))
