# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyPdb2pqr(PythonPackage):
    """
    PDB2PQR - determining titration states,
    adding missing atoms, and assigning
    charges/radii to biomolecules.
    """

    # Url for the package's homepage.
    homepage = "http://www.poissonboltzmann.org/"
    pypi     = "pdb2pqr/pdb2pqr-3.5.2.tar.gz"

    # List of GitHub accounts to
    # notify when the package is updated.
    maintainers = ['richtesn', 'thielblz']

    version('3.5.2', sha256='9d145ff3797a563ce818f9d2488413ac339f66c58230670c2455b2572cccd957')
    version('3.5.1', sha256='50272a850e876ea76d253e21eb01041ac8a01f7f17d73d0da95b26a6204c2564')
    version('3.5.0', sha256='560d992b62e009a6e752c8b7db3b53243ff08945eb76eb9460e43887433a55b1')
    version('3.4.1', sha256='9e8cabd984f8e31f010bda0a3e81c87ad93f403d1fd36c787627e5134f7542ba')

    depends_on('python@3.8:', type=('build','run'))
    depends_on('py-setuptools', type=('build'))
    depends_on('py-docutils', type=('build','run'))
    depends_on('py-mmcif-pdbx@1.1.2:', type=('build','run'))
    depends_on('py-numpy', type=('build','run'))
    depends_on('py-propka@3.2:', type=('build','run'))
    depends_on('py-requests', type=('build','run'))
    depends_on('py-pytest', type=('test'))
    depends_on('py-pandas@1.0:', type=('test'))
    depends_on('py-testfixtures', type=('test'))

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which('pytest')
        pytest()
