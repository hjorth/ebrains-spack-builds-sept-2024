# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyTvbMultiscale(PythonPackage):
    """
    Toolbox for Co-Simulation of TVB with NEST, NetPyNE and ANNarchy.
    """

    homepage = "https://github.com/the-virtual-brain/tvb-multiscale"
    url = "https://github.com/the-virtual-brain/tvb-multiscale/archive/refs/tags/2.1.0.ebrains.tar.gz"

    maintainers = ['dionperd', 'paulapopa', "ldomide", "vbragin"]

    version('2.1.0.ebrains', sha256='eab51e235f565cffbf300daa4b7c65a7afacde149e2eda0f49437db3c2429253')

    patch('tvb-multiscale-2.1.0-version-pep440.patch', when='@2.1.0.ebrains')

    # python_requires
    depends_on('python@3.8:3.11', type=('build', 'run'))

    # setup_requires
    depends_on('py-pip', type='build')
    depends_on('py-setuptools', type='build')

    # install_requires
    depends_on('py-tvb-framework', type=('build', 'run'))
    depends_on('py-tvb-contrib', type=('build', 'run'))
    depends_on('nest', type=('build', 'run'))
    depends_on('py-netpyne', type=('build', 'run'))
    depends_on('py-annarchy', type=('build', 'run'))  # TODO!
    depends_on('py-nestml', type=('build', 'run'))
    depends_on('py-elephant', type=('build', 'run'))
    depends_on('py-pyspike', type=('build', 'run'))
    depends_on('py-ray', type=('build', 'run'))

    # Test dependency
    depends_on('py-pytest', type='test')
    
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which('pytest')
        pytest('tvb_multiscale/tests')
