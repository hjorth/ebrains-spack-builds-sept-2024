# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *

class PyPythonKeycloak(PythonPackage):
    """python-keycloak is a Python package providing access to the Keycloak API"""

    homepage = "https://github.com/marcospereirampj/python-keycloak"
    url      = "https://files.pythonhosted.org/packages/py3/p/python_keycloak/python_keycloak-2.9.0-py3-none-any.whl"
    
    version('2.9.0', sha256='f2b42fc27b474ac791900eee38049ba9342613118f599e8214e85cfd58d4329e', expand=False)
    version('2.8.0', sha256='73baa30d5a45ac7e1167ad99ef39013cd7621c2cfc44745680b0351a3959e0c9', expand=False)
    version('2.7.0', sha256='e16897249c18e63782d3c88c8de36e1abf0aded6fbd93ad57866184fa25e3a84', expand=False)
    version('2.6.1', sha256='5da72c18640b9a02cb53c405673d764b8374a759bb36eba7c6aaf440c8449ede', expand=False)
    version('2.6.0', sha256='a1ce102b978beb56d385319b3ca20992b915c2c12d15a2d0c23f1104882f3fb6', expand=False)
    version('2.5.0', sha256='ed1c1935ceaf5d7f928b1b3ab945130f7d54685e4b17da053dbc7bfee0c0271e', expand=False)
    version('2.4.0', sha256='001d50f9aba5cafb012a5e6f4842ea97d59d12287a86fa3c848ad97db0c3c2bc', expand=False)
    version('2.3.0', sha256='b93d3d5a67d56da86a744f57b180e3d091dfd7859ea0b125363a3d9fccc0a47b', expand=False)
    version('2.2.0', sha256='c860bbdaadc28dabbe990daf71910448aa5e238d280ede9cd53f8b035869de37', expand=False)
    version('2.1.1', sha256='140d95ca08a2acae1f4cdaf6130bbba3ba3309f059326f7bebd87f150561c4b4', expand=False)

    depends_on('py-poetry-core', type='build')
    depends_on('py-requests', type=('build', 'run'))
    depends_on('py-python-jose', type=('build', 'run'))
    depends_on('py-urllib3', type=('build', 'run'))
    depends_on('py-requests-toolbelt', type=('build', 'run'))
