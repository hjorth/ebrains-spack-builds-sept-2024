# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyTvbData(PythonPackage):
    """
    Various demonstration datasets for use with The Virtual Brain are provided here.
    """

    homepage = "https://zenodo.org/records/10128131"

    maintainers = ['paulapopa', "ldomide"]

    version('2.8.1',
            '08ae19833ba8ac158c91fbcb988b9bf0',
            url='https://zenodo.org/records/10128131/files/tvb_data.zip')
    version('2.8',
            'd2f9b5933327c1d106838d301a4bf7b5',
            url='https://zenodo.org/records/8331301/files/tvb_data.zip')

    # python_requires
    depends_on('python', type=('build', 'run'))

    # setup_requires
    depends_on('py-pip', type='build')
    depends_on('py-setuptools', type=('build'))

    # this is only a data holder package, thus no real python code exists inside.
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):

        with working_dir('spack-test', create=True):
            python('-c',
                   'import tvb_data; '
                   'import os; '
                   'RR = tvb_data.__path__[0]; '
                   'assert os.path.exists(os.path.join(RR, "Default_Project.zip")); '
                   'assert os.path.exists(os.path.join(RR, "connectivity"))'
                   '')
