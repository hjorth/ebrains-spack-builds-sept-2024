# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *

class Acpype(PythonPackage):
    """A tool based in Python to use Antechamber to generate topologies for chemical 
    compounds and to interface with others python applications like CCPN and ARIA"""

    # Homepage and download url
    homepage = "https://github.com/alanwilter/acpype"
    git = 'https://github.com/alanwilter/acpype.git'
    url = 'https://github.com/alanwilter/acpype/archive/refs/tags/2022.7.21.tar.gz'

    # Set the gitlab accounts of this package maintainers
    maintainers = ['dbeltran']

    # Versions
    version('master', branch='master')
    version('2022.7.21', sha256='5f7e6162d9a0aed2f770b9ccf5617ac1398a423cca815ae37cbf66d4cd62ea2f')

    # Dependencies
    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('ambertools')
    depends_on('openbabel')
    depends_on('py-poetry-core')

    # Test
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install (self):
        python("-c", 'import acpype')
