# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyMorphounit(PythonPackage):
    """A SciUnit library for data-driven testing of neuronal morphologies
    """

    homepage = "https://github.com/appukuttan-shailesh/morphounit/"
    pypi     = "morphounit/morphounit-1.0.4.tar.gz"
    git      = "https://github.com/appukuttan-shailesh/morphounit.git"

    version('1.0.4', sha256='6dacadc91c66c15af91da8c1d45977e4aeaaa297a7bbc2e679f7d6a5e54e6f32')

    depends_on('python@3.6.9:')
    depends_on('py-setuptools',       type=('build'))
    depends_on('py-neo@0.8.0:',       type=('build', 'run'))
    depends_on('py-elephant@0.4.1:',  type=('build', 'run'))
    depends_on('py-sciunit@0.2.5.1:', type=('build', 'run'))
    depends_on('py-neurom@1.4.10',    type=('build', 'run'))
    depends_on('py-tabulate@0.8.6:',  type=('build', 'run'))
    depends_on('py-seaborn@0.10.0:',  type=('build', 'run'))
