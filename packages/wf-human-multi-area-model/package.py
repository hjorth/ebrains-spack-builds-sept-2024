# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class WfHumanMultiAreaModel(Package):
    """Meta-package to collect all dependencies of the Human Multi-Area-Model."""

    git = "https://github.com/INM-6/human-multi-area-model"
    maintainer = ["rshimoura", "terhorstd"]

    version("2.0.1",  tag="v2.0.1")
    version("master",  branch="master")

    depends_on("nest@3.8:", type=("run", "test"))
    depends_on("py-joblib@1.2.0:", type=("run", "test"))
    depends_on("py-xlrd@2.0.1:", type=("run", "test"))
    depends_on("py-matplotlib@3.7.3:", type=("run", "test"))
    depends_on("py-pyyaml@6.0:", type=("run", "test"))
    depends_on("py-numpy@1.23.5:", type=("run", "test"))
    depends_on("py-seaborn@0.12.2:", type=("run", "test"))
    depends_on("python@3.8:", type=("run", "test"))
    depends_on("py-pandas@2.0.3:", type=("run", "test"))
    depends_on("py-scipy@1.10.1:", type=("run", "test"))
    depends_on("py-nnmt@1.3.0:", type=("run", "test"))
    depends_on("py-dicthash@0.0.1:", type=("run", "test"))
    depends_on("py-networkx@3.1:", type=("run", "test"))
    depends_on("py-notebook@6.4:", type=("run", "test"))
    depends_on("py-future@0.18.2:", type=("run", "test"))

    def install(self, spec, prefix):
        install_tree(".", join_path(prefix, "notebooks"))

    def _nbconvert(self, nb, nb_out):
        jupyter = Executable("jupyter")
        args = [
            "nbconvert",
            "--ExecutePreprocessor.kernel_name=python3",
            "--execute",
            "--to",
            "notebook",
            nb,
            "--output",
            nb_out
        ]
        try:
            # execute notebook and save
            jupyter(*args)
        except Exception as e:
            # if the above fails, re-run notebook to produce output with error
            jupyter(*(args+["--allow-errors"]))
            raise

    def _run_notebooks(self, output_dir):
        mkdirp(output_dir)
        self._nbconvert(join_path(self.prefix, "notebooks", "humam_tutorial.ipynb"), join_path(output_dir, "humam_tutorial.ipynb"))

    @run_after("install")
    @on_package_attributes(run_tests=True)
    def installcheck(self):
        self._run_notebooks(join_path(self.stage.path, ".install_time_tests"))
        copy_tree(join_path(self.stage.path, ".install_time_tests"), join_path(self.prefix, '.build'))

    def test_notebook(self):
        self._run_notebooks(join_path(self.test_suite.stage, self.spec.format("out-{name}-{version}-{hash:7}")))
