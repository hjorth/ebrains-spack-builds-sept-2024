# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyTvbFramework(PythonPackage):
    """
    "The Virtual Brain" Project (TVB Project) has the purpose of offering modern tools to the Neurosciences community,
    for computing, simulating and analyzing functional and structural data of human brains, brains modeled at the level
    of population of neurons.
    """

    homepage = "https://www.thevirtualbrain.org/"
    pypi = 'tvb-framework/tvb-framework-2.9.tar.gz'

    maintainers = ['paulapopa', 'ldomide']

    version('2.9', '44f102bcfd63a09c9c2af662a004415f2fc64847c0376218011b27fd2bb03848')
    version('2.8.2', '00f4bdfc9faccf2df1a95695e231d7d4890cbf5ca7eac0474d52b976a51d91d5')
    version('2.8.1.1', '5f858398708453b3ea48de11c4ca1f098fe732738431ed421cc435d9bfc4b6fb')
    version('2.7.3.1', 'e1de0df0b8f43a317962b066790a5d724e1e6ae98ee124ad8b977f92aa228877')
    version('2.7.2', '1109c956c22e737d276bc822a69716c811d22e12f0606b47cc61700067af3ff4')
    version('2.7.1', '4645a489f24bc877370b213819bf4d31fbc4104bbfd0f3abde91a8d56635ee54')
    version('2.7', '6c16d95071283c3866660132f3f0ea789100d0e3b4c57daacbdb63506be7e29f')
    version('2.4', 'af75c07e35565cfcf12e6a9052934c58a45c855455cec2f2cd3bd47fdf90e31d')
    version('2.3', '0f3386135cdbd80bfd7d31f2e056b015a27ff6d081492db16274deed581b0aac')

    # python_requires
    depends_on('python@3.8:', type=('build', 'run'))

    # setup_requires
    depends_on('py-setuptools', type='build')

    # install_requires
    depends_on('py-alembic', type=('build', 'run'))
    depends_on('py-bct', type=('build', 'run'))
    depends_on('py-cherrypy', type=('build', 'run'))
    depends_on('py-docutils', type=('build', 'run'))
    depends_on('py-formencode', type=('build', 'run'))
    depends_on('py-flask', type=('build', 'run'))
    depends_on('py-flask-restx', type=('build', 'run'))
    depends_on('py-h5py', type=('build', 'run'))
    depends_on('py-jinja2', type=('build', 'run'))
    depends_on('py-matplotlib', type=('build', 'run'))
    depends_on('py-nibabel', type=('build', 'run'))
    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-pandas', type=('build', 'run'))
    depends_on('py-pillow', type=('build', 'run'))
    depends_on('py-psutil', type=('build', 'run'))
    depends_on('py-python-keycloak', type=('build', 'run'))
    depends_on('py-requests', type=('build', 'run'))
    depends_on('py-scikit-learn', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-siibra@1.0:', type=('build', 'run'))
    depends_on('py-simplejson', type=('build', 'run'))
    depends_on('py-six', type=('build', 'run'))
    depends_on('py-sqlalchemy', type=('build', 'run'))
    depends_on('py-tvb-data', type=('run', 'test'))
    depends_on('py-tvb-gdist', type=('run', 'test'))
    depends_on('py-tvb-library', type=('build', 'run'))
    depends_on('py-tvb-storage', type=('build', 'run'))
    depends_on('py-werkzeug', type=('build', 'run'))

    depends_on('py-pytest', type='test')
    depends_on('py-pytest-benchmark', type='test')
    depends_on('py-pytest-mock', type='test')
    depends_on('py-beautifulsoup4', type='test')

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which('pytest')
        pytest('--ignore', 'tvb/tests/framework/adapters/creators/siibra_base_test.py',
               '--ignore', 'tvb/tests/framework/adapters/creators/siibra_creator_test.py')

