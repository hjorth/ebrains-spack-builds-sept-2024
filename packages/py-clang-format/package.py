# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyClangFormat(PythonPackage):
    """FIXME: Put a proper description of your package here."""

    homepage = "https://pypi.org/project/clang-format"
    url      = "https://files.pythonhosted.org/packages/17/fd/723876a1e55397e4b060f2e9e3d4a5e4820f6e09ea05fe8c8cf4ddfd1ae8/clang_format-13.0.1-py2.py3-none-manylinux_2_12_x86_64.manylinux2010_x86_64.whl"

    maintainers = ['terhorstd']

    version('13.0.1', url='https://files.pythonhosted.org/packages/17/fd/723876a1e55397e4b060f2e9e3d4a5e4820f6e09ea05fe8c8cf4ddfd1ae8/clang_format-13.0.1-py2.py3-none-manylinux_2_12_x86_64.manylinux2010_x86_64.whl', sha256='58e91debc2b2d14d174c73c678ffac676cb171152ee3f4239b6cbe6975e4ede1', expand=False)
    version('9.0.0', url='https://files.pythonhosted.org/packages/25/f2/d989afaf8a91385f18fd06e6202644be0bc3a1d14548c18a8ece4911e005/clang_format-9.0.0-py2.py3-none-manylinux1_x86_64.whl', sha256='085342f9e238c9b03a019e20ec23f242f8795a3cca9296ab2427b1dea45e7014', expand=False)

    # FIXME: Only add the python/pip/wheel dependencies if you need specific versions
    # or need to change the dependency type. Generic python/pip/wheel dependencies are
    # added implicity by the PythonPackage base class.
    # depends_on('python@2.X:2.Y,3.Z:', type=('build', 'run'))
    # depends_on('py-pip@X.Y:', type='build')
    # depends_on('py-wheel@X.Y:', type='build')

    # FIXME: Add a build backend, usually defined in pyproject.toml. If no such file
    # exists, use setuptools.
    # depends_on('py-setuptools', type='build')
    # depends_on('py-flit-core', type='build')
    # depends_on('py-poetry-core', type='build')

    # FIXME: Add additional dependencies if required.
    # depends_on('py-foo', type=('build', 'run'))

    #def global_options(self, spec, prefix):
    #    # FIXME: Add options to pass to setup.py
    #    # FIXME: If not needed, delete this function
    #    options = []
    #    return options

    #def install_options(self, spec, prefix):
    #    # FIXME: Add options to pass to setup.py install
    #    # FIXME: If not needed, delete this function
    #    options = []
    #    return options
