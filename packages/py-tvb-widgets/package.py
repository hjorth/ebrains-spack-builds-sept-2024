# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyTvbWidgets(PythonPackage):
    """
    "The Virtual Brain" Project (TVB Project) has the purpose of offering modern tools to the Neurosciences community,
    for computing, simulating and analyzing functional and structural data of human brains, brains modeled at the level
    of population of neurons.
    """

    homepage = "https://www.thevirtualbrain.org/"
    pypi = 'tvb-widgets/tvb-widgets-2.1.0.tar.gz'

    maintainers = ['ldomide', 'paulapopa', 'teodoramisan']

    version('2.1.0', '8d0cdd121045bd93fca1d71b7ffe8ce9421cf069388ad56d1e4ec1d09f31a8d7')
    version('2.0.3', '33bd94bef5b49df6843a022342d4c4c0d080851d638b5d07295880298cd6fb00')

    # python_requires
    depends_on('python@3.8:', type=('build', 'run'))

    # setup_requires
    depends_on('py-setuptools', type='build')

    # install_requires
    depends_on('py-colorcet', type=('build', 'run'))
    depends_on('py-ebrains-drive', type=('build', 'run'))
    depends_on('py-ipympl@0.8.5:', type=('build', 'run'))
    depends_on('py-ipywidgets', type=('build', 'run'))
    depends_on('py-ipython', type=('build', 'run'))
    depends_on('py-ipycanvas', type=('build', 'run'))
    depends_on('py-joblib', type=('build', 'run'))
    depends_on('py-mne@1.0:', type=('build', 'run'))
    depends_on('py-numpy@:1', type=('build', 'run'))
    depends_on('py-plotly', type=('build', 'run'))
    depends_on('py-pyvista@0.43.0:', type=('build', 'run'))
    depends_on('py-requests', type=('build', 'run'))
    depends_on('py-tvb-data', type=('run', 'test'))
    depends_on('py-tvb-library', type=('build', 'run'))
    depends_on('py-tvb-framework', type=('build', 'run'))
    depends_on('py-pyunicore@1.0.0:', type=('build', 'run'))
    depends_on('py-traitlets@5.7.1:', type=('build', 'run'))
    depends_on('py-toml', type=('build', 'run'))
    depends_on('py-bokeh', type=('build', 'run'))
    depends_on('vtk@9:9.3', type=('build', 'run'))

    depends_on('py-pytest', type='test')
    depends_on('py-pytest-mock', type='test')

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        with working_dir('spack-test', create=True):
            python('-c',
                   'import tvbwidgets; '
                   'from tvbwidgets.ui.pse_widget import PSEWidget')

