# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyOdetoolbox(PythonPackage):
    """A Python package for the automatic selection and generation of
    integration schemes for systems of ordinary differential equations
    """

    homepage = 'https://ode-toolbox.readthedocs.io'
    url      = 'https://pypi.org/packages/py3/o/odetoolbox/odetoolbox-2.5-py3-none-any.whl'
    git      = 'https://github.com/nest/ode-toolbox'

    maintainers = ['clinssen']

    version('2.5.4', sha256='eadb8f42a553cb34180a11d96cbac49ec4d0f07caf0358e066ab63415746e009', expand=False)
    version('2.5', sha256='947bbb289830dde60066106f5f628de2598a71129eafff8ec86317a0b5e8960e', expand=False)

    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('py-pip', type='build')
    depends_on('py-sympy@:1.4,1.7,1.8,1.9,1.10.1:', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-numpy@1.8.2:', type=('build', 'run'))
    depends_on('py-cython', type=('build', 'run'))
    depends_on('py-matplotlib', type=('build', 'run'))
    depends_on('py-setuptools', type=('build', 'run'))
    depends_on('py-pytest', type='test')
