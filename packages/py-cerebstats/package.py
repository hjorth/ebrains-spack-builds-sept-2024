# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyCerebstats(PythonPackage):
    """CerebStats is the statistical library for CerebUnit.
    It is one of the four components for validating cerebellum models:
    CerebModels and CerebData do not require installation while CerebUnit and CerebStats require installation."""

    homepage = "https://cerebstats.readthedocs.io"
    pypi     = "cerebstats/cerebstats-0.0.3.tar.gz"
    git = "https://github.com/cerebunit/cerebstats.git"
    
    maintainers = ['lungsi', 'apdavison'] # github usernames

    version('0.0.3', sha256='0270ac7d37706d067e5e72de80f6dbd23309ec02a71ad4443f0f457790a9aa8c')

    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-quantities', type=('build', 'run'))
    depends_on('py-sciunit', type=('build', 'run'))
    
