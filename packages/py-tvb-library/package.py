# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyTvbLibrary(PythonPackage):
    """
    "The Virtual Brain" Project (TVB Project) has the purpose of offering modern tools to the Neurosciences community,
    for computing, simulating and analyzing functional and structural data of human brains, brains modeled at the level
    of population of neurons.
    """

    homepage = "https://www.thevirtualbrain.org/"
    pypi = 'tvb-library/tvb_library-2.9.2.tar.gz'

    maintainers = ['ldomide', 'paulapopa', 'teodoramisan']

    version('2.9.2', 'be3562b28464e51d09e9378d71996fc11522121124c5438dd3e9cdd71060d09e')
    version('2.9.1', 'a251f602bffe768e6184ffc14d3d6ad9f03be879a78062a7397c30a5f8878b41')
    version('2.9', '0a65fa3d803ef8da3d69a0f27a13e40b79876ee6d5b627c34d0c9fc5c12697e6')
    version('2.8.2', '4222dadbecce53b29e2141801b1ca8eb99c4595ace6c7d0469f7e0a41d3f384a')
    version('2.8.1', 'be130b10e7e176d503709afd211ae728f45d4e8b4a6082c6f6fcfbc1be990d0c')
    version('2.8', '49624cc8ca0673f8590227a9246ea2ffa855ef7a4242235ce85263ff6f3db7c8')
    version('2.7.3', 'bcf7ad291ff4549508f6720e207561f5f0bad638cd0d8507fd4ed9a7bcc561ff')
    version('2.7.2', 'cc93c15a3b2c59e9e808d099466b3cb38a07ef81f693f32f24d9a94c0cedd39f')
    version('2.7.1', '971f6a354231f7608484b17aa37ac053374b7beeab9c4676d3d28227f1fb100e')
    version('2.7', '0b28ee0221818f677ee26bd3cd590e42ccdfb6dea705bd78abf4788779f53f7d')
    version('2.6', '3912f6881688f2ec6eda31d179d630922257a861cf6f161e90aea831cfc63583')
    version('2.5', 'd3fc26cdee145d73c459eff9172fd5b8eeb4ef264a146a1f0aca062993123793')
    version('2.4', 'f90a34a94310cb8e709d9ff888e0d066c1b5549a117ecf65516bae1872c5d0c9')
    version('2.3', '0595f2eca95e5ed4c7a1c88425914cc71d0ea7a9f4ec575b6a315ca2408ea801')

    # python_requires
    depends_on('python@3.8:', type=('build', 'run'))

    # build_requires
    depends_on('py-hatchling', type='build')

    # install_requires
    depends_on('py-autopep8', type=('build', 'run'))
    depends_on('py-deprecated', type=('build', 'run'))
    depends_on('py-docutils', type=('build', 'run'))
    depends_on('py-importlib-metadata', type=('build','run'))
    depends_on('py-ipywidgets', type=('build', 'run'))
    depends_on('py-lxml', type=('build', 'run'))
    depends_on('py-lems', type=('build', 'run'))
    depends_on('py-mako', type=('build', 'run'))
    depends_on('py-matplotlib', type=('build', 'run'))
    depends_on('py-networkx', type=('build', 'run'))
    depends_on('py-numpy@:1', type=('build', 'run'))
    depends_on('py-numba', type=('build', 'run'))
    depends_on('py-numexpr', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-scipy@:1.13', type=('build', 'run'), when='@:2.9.1')
    depends_on('py-six', type=('build', 'run'))
    depends_on('py-tvb-data', type=('run', 'test'))
    depends_on('py-tvb-gdist', type=('run', 'test'))

    depends_on('py-pytest', type='test')
    depends_on('py-h5py', type='test')
    depends_on('py-pytest-benchmark', type='test')
    depends_on('py-pytest-xdist', type='test')

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which('pytest')
        pytest()
