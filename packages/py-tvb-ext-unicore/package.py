from spack.package import *
import os


class PyTvbExtUnicore(PythonPackage):
    """"
    This jupyter extension offers a UI component to monitor HPC jobs through Unicore interface.
    It allows users to easily switch between computing sites, retrieve details about the jobs, and also cancel them.
    """

    homepage = "https://www.thevirtualbrain.org/"
    pypi = "tvb-ext-unicore/tvb-ext-unicore-2.0.0.tar.gz"
    maintainers = ['ldomide', 'adrianciu']

    version("2.0.0", sha256="85c4176c4a833149245e41e2448e94e5507f44bb7c3ffe1e5cf52f9c9b76e7ad")

    depends_on('py-setuptools', type='build')
    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('py-hatchling@1.5:', type='build')
    depends_on('py-hatch-nodejs-version@0.3.1:', type=('build', 'run'))
    depends_on('py-hatch-jupyter-builder@0.5:', type='build')
    depends_on('py-jupyterlab@3.4.7:3', type=('build', 'run'))
    depends_on('py-jupyter-server', type=('build', 'run'))
    depends_on('py-pyunicore@1.0:', type=('build', 'run'))
    depends_on('py-jupyter-packaging@0.10:', type='build')
    depends_on('npm', type='build')
    depends_on('node-js', type='build')

    depends_on('py-pytest', type='test')
    depends_on('py-pytest-mock', type='test')

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        os.environ['CLB_AUTH'] = 'test_auth_token'
        pytest = which('pytest')
        pytest()
