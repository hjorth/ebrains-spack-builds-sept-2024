# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *

class PyPytensor(PythonPackage):
    """Python library that allows one to define, optimize, and efficiently evaluate mathematical expressions involving
    multi-dimensional arrays. It provides the computational backend for PyMC."""

    homepage = "https://github.com/pymc-devs/pytensor"
    pypi = "pytensor/pytensor-2.27.1.tar.gz"

    version("2.27.1", "ed5075e1504e0e4c2322340111289820c5e1718b70187922777d560a8ef26f75")

    depends_on("python@3.10:3.13", type=("build", "run"))
    depends_on("py-setuptools@59.0.0:", type="build")
    depends_on("py-cython", type="build")
    depends_on("py-versioneer+toml", type="build")
    depends_on("py-scipy@1.0:1", type=("build", "run"))
    depends_on("py-numpy@1.17.0:", type=("build", "run"))
    depends_on("py-filelock", type=("build", "run")) # TODO: it needs filelock>=3.15, but on pypi the latest one is 3.12.4
    depends_on("py-etuples", type=("build", "run"))
    depends_on("py-logical-unification", type=("build", "run"))
    depends_on("py-mini-kanren", type=("build", "run"))
    depends_on("py-cons", type=("build", "run"))
