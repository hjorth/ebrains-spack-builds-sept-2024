# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os
import unittest.mock
import xml.etree.ElementTree as ET

from spack.package import *
from spack.util.environment import EnvironmentModifications
import spack.build_environment

import importlib
build_brainscales = importlib.import_module("spack.pkg.ebrains-spack-builds.build_brainscales")


class PynnBrainscales(build_brainscales.BuildBrainscales):
    """PyNN toplevel for the BrainScaleS-2 neuromorphic hardware systems"""

    homepage = "https://github.com/electronicvisions/pynn-brainscales"
    # This repo provides a custom waf binary used for the build below
    git      = "https://github.com/electronicvisions/pynn-brainscales.git"

    maintainers = ["emuller", "muffgaga"]

    # newer versions are defined in the common base package
    version('8.0-a5',         tag='pynn-brainscales-8.0-a5')
    version('8.0-a4',         tag='pynn-brainscales-8.0-a4')
    version('8.0-a3',         tag='pynn-brainscales-8.0-a3')
    version('8.0-a2',         tag='pynn-brainscales-8.0-a2')
    version('8.0-a1',         tag='pynn-brainscales-8.0-a1')
    version('7.0-rc1-fixup3', tag='pynn-brainscales-7.0-rc1-fixup3')
    version('7.0-rc1-fixup2', tag='pynn-brainscales-7.0-rc1-fixup2')
    version('7.0-rc1-fixup1', branch='waf')

    # compiler for the BrainScaleS-2 embedded processor ("PPU"); needed for
    # building/linking, at runtime and for testing
    depends_on('oppulance@8.0-a5', when='@8.0-a5', type=('build', 'link', 'run', 'test'))
    depends_on('oppulance@8.0-a4', when='@8.0-a4', type=('build', 'link', 'run', 'test'))
    depends_on('oppulance@8.0-a3', when='@8.0-a3', type=('build', 'link', 'run', 'test'))
    depends_on('oppulance@8.0-a2', when='@8.0-a2', type=('build', 'link', 'run', 'test'))
    depends_on('oppulance@8.0-a1', when='@8.0-a1', type=('build', 'link', 'run', 'test'))
    depends_on('oppulance@7.0-rc1-fixup3', when='@7.0-rc1-fixup3', type=('build', 'link', 'run', 'test'))
    depends_on('oppulance@7.0-rc1-fixup2', when='@7.0-rc1-fixup2', type=('build', 'link', 'run', 'test'))
    depends_on('oppulance@7.0-rc1-fixup1', when='@7.0-rc1-fixup1', type=('build', 'link', 'run', 'test'))

    # host software dependencies
    depends_on('bitsery', type=('build', 'link', 'run', 'test'))
    depends_on('binutils+gold+ld+plugins', type=('build', 'link', 'run')) # specialize
    depends_on('boost@1.69.0: +graph+icu+mpi+numpy+coroutine+context+filesystem+python+serialization+system+thread+program_options cxxstd=17', type=('build', 'link', 'run', 'test'))
    depends_on('cereal', type=('build', 'link', 'run', 'test'))
    depends_on('cppcheck', type=('build', 'link', 'run'))
    depends_on('genpybind@ebrains-llvm15', type=('build', 'link'))
    depends_on('gflags', type=('build', 'link', 'run'))
    depends_on('googletest@1.11.0:+gmock', type=('build', 'link', 'run')) # variadic templates needed
    depends_on('inja', type=('build', 'link', 'run', 'test')) # template engine for PPU source jit generation
    depends_on('intel-tbb', type=('build', 'link', 'run'))  # ppu gdbserver
    depends_on('libelf', type=('build', 'link', 'run'))
    depends_on('liblockfile', type=('build', 'link', 'run'))
    depends_on('log4cxx@0.12.1:1.0', when="@:8.0-a3", type=('build', 'link', 'run'))
    depends_on('log4cxx@1.1: +events_at_exit', when="@8.0-a4:", type=('build', 'link', 'run'))
    depends_on('pkgconfig', type=('build', 'link', 'run'))
    depends_on('psmisc', type=('run', 'test'))
    depends_on('python@3.7.0:', type=('build', 'link', 'run')) # BrainScaleS-2 only supports Python >= 3.7
    depends_on('py-deap@1.3.1:', type=('build', 'link', 'run'))
    depends_on('py-h5py', type=('build', 'link', 'run')) # PyNN tests need it
    depends_on('py-matplotlib', type=('build', 'link', 'run'))
    depends_on('py-nose', type=('build', 'link', 'run'))
    depends_on('py-numpy', type=('build', 'link', 'run'))
    depends_on('py-pybind11', type=('build', 'link', 'run'))
    depends_on('py-pybind11-stubgen', type=('build', 'link', 'run'))
    depends_on('py-pycodestyle', type=('build', 'link', 'run'))
    depends_on('py-pyelftools', type=('build', 'link', 'run'))
    depends_on('py-pylint', type=('build', 'link', 'run'))
    depends_on('py-pynn@0.9.4:', type=('build', 'link', 'run'))
    depends_on('py-pytest', type=('build', 'link', 'run'))
    depends_on('py-pyyaml', type=('build', 'link', 'run'))
    depends_on('py-scipy', type=('build', 'link', 'run'))
    depends_on('py-sqlalchemy', type=('build', 'link', 'run'))
    depends_on('util-linux', type=('build', 'link', 'run'))
    depends_on('yaml-cpp+shared', type=('build', 'link', 'run'))
    extends('python')

    # some versions of dependencies are broken
    conflicts("boost@1.86.0") # sha1 digest changed length, but boost::compute didn't adapt

    def install_test(self):
        with working_dir('spack-test', create=True):
            old_pythonpath = os.environ.get('PYTHONPATH', '')
            os.environ['PYTHONPATH'] = ':'.join([str(self.prefix.lib), old_pythonpath])
            bash = which("bash")
            # ignore segfaults for now (exit code 139)
            bash('-c', '(python -c "import pynn_brainscales; print(pynn_brainscales.__file__)" || ( test $? -eq 139 && echo "segfault")) || exit $?')
