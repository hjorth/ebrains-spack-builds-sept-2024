# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyArborize(PythonPackage):
    """
    A framework to package NEURON cell models following a clear separation between cell model description and its implementation in NEURON.
     Cell models described using this framework are highly portable, testable and distributable.
    """

    homepage = "https://github.com/dbbs-lab/arborize"
    pypi = "arborize/arborize-4.1.0.tar.gz"

    license("GPL-3.0-only")
    maintainers = ["helveg","filimarc","drodarie"]

    version("4.1.0", sha256="2cb88b890fa69de42a49b38ea4defd0caa4ee91b34f9b75216e1536d9058f57f")

    depends_on("py-flit-core@3.2:4.0", type="build")
    depends_on("py-numpy@1.21:")
    depends_on("py-errr@1.2:")
    depends_on("py-morphio@3.3.6:4")
