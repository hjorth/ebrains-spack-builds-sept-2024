# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyPynwb(PythonPackage):
    """A Python API for working with Neurodata stored in the NWB Format."""

    homepage = "https://github.com/NeurodataWithoutBorders/pynwb"
    pypi = "pynwb/pynwb-2.1.0.tar.gz"

    maintainers = ["bendichter"]

    version("2.1.0", sha256="53161341ff970a99de4f362e7b16034b9661797ad7ed65e062649cc6655f350b")

    depends_on("py-setuptools", type="build")

    depends_on("py-h5py@2.10:", type=("build", "run"))
    depends_on("py-hdmf@3.4.0:", type=("build", "run"))
    depends_on("py-numpy@1.16:", type=("build", "run"))
    depends_on("py-pandas@1.1.5:", type=("build", "run"))
    depends_on("py-python-dateutil@2.7.3:", type=("build", "run"))
