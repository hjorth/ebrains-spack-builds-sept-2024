# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyTorchtestcase(PythonPackage):
    """Extends unittest.TestCase such that assertions support PyTorch tensors and parameters."""

    homepage = "https://github.com/phohenecker/torch-test-case"
    pypi = "torchtestcase/torchtestcase-2018.2.tar.gz"

    version("2018.2", sha256="0061cde2eb79f09c9501fae675c52c799371606d52afcff8753c44e1a6254a00")
    version("2018.1", sha256="691b053b0466aed40201e1b41f5a903b4df889a64272a18bcab4b1c8e9091cb4")
    version("2017.1", sha256="f8bb0c4e3216087130f80c4237bb5e4c1e6de629d553f25fd7b85f6e33bf9b34")

    depends_on("py-numpy@1.13.1:", type=("build", "run"))
    depends_on("py-torch@0.4.0:", type=("build", "run"))
