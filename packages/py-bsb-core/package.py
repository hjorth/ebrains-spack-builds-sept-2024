# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyBsbCore(PythonPackage):
    """
    The BSB is a component framework for neural modeling, which focuses on component
    declarations to piece together a model.
    """

    homepage = "https://bsb.readthedocs.io"
    pypi = "bsb-core/bsb_core-5.0.0.tar.gz"

    license("GPL-3.0-only")
    maintainers = ["helveg","filimarc","drodarie"]

    version("5.0.2", sha256="414be0f3ba72b2f656b89f8e4636e4a1d19b1f4dc9ba9360cc984020cb1859dc")
    version("5.0.1", sha256="7cb905ee38419709b4ead2ffb40e1005d813d2c6780706b3f5eb2696aabeb983")
    version("5.0.0", sha256="08e1776d351a8bb5c056ffbd8108d0bd941f71518b475aecbad9f22050b7cc91")

    variant('parallel', default=True,
            description='Build with MPI bindings')

    depends_on("python@3.9:3.12", type=("build", "run"))
    depends_on("py-flit-core@3.2:4.0", type="build")
    depends_on("py-numpy@1.19:")
    depends_on("py-scipy@1.5:")
    depends_on("py-scikit-learn@1.0:")
    depends_on("py-rtree@1.0:")
    depends_on("py-psutil@5.8:")
    depends_on("py-pynrrd@1.0:")
    depends_on("py-toml@0.10:")
    depends_on("py-requests")
    depends_on("py-urllib3@2:")
    depends_on("py-appdirs@1.4:")
    depends_on("py-neo")
    depends_on("py-tqdm@4.50:")
    depends_on("py-shortuuid")
    depends_on("py-quantities@0.15.0:")
    depends_on("py-morphio@3.3:")
    depends_on("py-errr@1.2.0:")
    depends_on("py-dashing@0.1.0:")
    depends_on("py-exceptiongroup")
    
    depends_on('mpi', when='+parallel')
    depends_on('py-mpi4py', when='+parallel')
    depends_on('py-mpipool@2.2.1:3', when='+parallel')
    depends_on('py-mpilock@1.1:', when='+parallel')


    def setup_build_environment(self, env):
        env.set("SPATIALINDEX_C_LIBRARY", self.spec["libspatialindex"].libs[0])

    def setup_run_environment(self, env):
        self.setup_build_environment(env) 
