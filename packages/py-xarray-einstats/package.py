# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyXarrayEinstats(PythonPackage):
    """Stats, linear algebra and einops for xarray"""

    homepage = "https://github.com/arviz-devs/xarray-einstats"
    pypi = "xarray_einstats/xarray_einstats-0.8.0.tar.gz"

    version("0.8.0", sha256="7f1573f9bd4d60d6e7ed9fd27c4db39da51ec49bf8ba654d4602a139a6309d7f")
    version("0.7.0", sha256="2d7b571b3bbad3cf2fd10c6c75fd949d247d14c29574184c8489d9d607278d38")
    version("0.6.0", sha256="ace90601505cfbe2d374762e674557ed14e1725b024823372f7ef9fd237effad")

    depends_on("python@3.10:", type=("build", "run"))
    depends_on("py-flit-core@3.4:4", type="build")
    depends_on("py-numpy@1.23:", type=("build", "run"))
    depends_on("py-scipy@1.9:", type=("build", "run"))
    depends_on("py-xarray@2022.09:", type=("build", "run"))
