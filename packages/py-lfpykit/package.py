# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyLfpykit(PythonPackage):
    """A Python package containing different electrostatic forward models for
    computing extracellular signals from multicompartment neurons
    """

    homepage = 'https://LFPykit.readthedocs.io'
    pypi = "LFPykit/LFPykit-0.5.tar.gz"
    git = 'https://github.com/LFPy/LFPykit.git'

    maintainers = ['espenhgn']

    version('0.4', sha256='ce92f5a987535ee72a76644bac90393eaa669d952f493dc388f3deb1c36f474d')
    version('0.5', sha256='9a7ae80ad905bb8dd0eeab8517b43c3d5b4fff2b8766c9d5a36320a7a67bd545')
    version('0.5.1', sha256='a962029460c8173c4fec3923204e04d64c370b05740b64c2d07efbe29cbe63a6')

    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('py-pip', type='build')
    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-setuptools', type=('build', 'run'))
    depends_on('py-meautility@1.5.1', type=('build', 'run'))
    depends_on('py-pytest', type='test')
