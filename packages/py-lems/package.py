# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyLems(PythonPackage):
    """
    Requirement necessary for py-tvb-library package.
    """

    homepage = "https://pypi.org/project/PyLEMS/"
    pypi = 'PyLEMS/PyLEMS-0.6.7.tar.gz'

    maintainers = ['paulapopa', 'ldomide']

    version('0.6.7', 'b82b66dbe51206b6c3db915651aadfeb6e8eeb5a95d3114ca8b97725f2fafce6')
    version('0.6.0', '809cb1186be18fc0390f9656677847d3a2666cfae816ed82897cc91dfdfa8a1c')

    # python_requires
    depends_on('python@3.8:', type=('build', 'run'))

    # setup_requires
    depends_on('py-pip', type=('build'))
    depends_on('py-setuptools', type=('build', 'run'))
