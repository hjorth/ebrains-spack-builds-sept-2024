# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class WfBiobb(BundlePackage):
    """Meta-package to collect all dependencies for the BioBB demos/tutorials."""

    homepage="https://wiki.ebrains.eu/bin/view/Collabs/biobb"

    maintainers = ["dbeltran"]

    version("0.1")

    depends_on("py-plotly")
    depends_on("py-nglview")
    depends_on("py-simpletraj")
