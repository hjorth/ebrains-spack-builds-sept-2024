# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyJaracoCollections(PythonPackage):
    """Collection objects similar to those in stdlib by jaraco"""

    homepage = "https://github.com/jaraco/jaraco.collections"
    pypi = "jaraco.collections/jaraco.collections-5.1.0.tar.gz"

    license("MIT")

    version("5.0.1", sha256="808631b174b84a4e2a592490d62f62dfc15d8047a0f715726098dc43b81a6cfa")
    version("5.0.0", sha256="1680e8d09f295f625c7ba926880175a26fdbe7092b4c76d198e30476b21cfe68")
    version("4.3.0", sha256="74ffc23fccfee4de0a2ebf556a33675b6a3c003d6335947d3122a0bc8822c8e4")

    depends_on("python@3.8:", type=("build", "run"))

    depends_on("py-setuptools@56:", type="build")
    depends_on("py-setuptools-scm@3.4.1: +toml", type="build")    
    depends_on("py-jaraco-text", type=("build", "run"))

