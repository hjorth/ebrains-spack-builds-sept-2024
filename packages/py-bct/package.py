# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyBct(PythonPackage):
    """
    Requirement necessary for py-tvb-framework package.
    """

    homepage = "https://pypi.org/project/bctpy/"
    pypi = 'bctpy/bctpy-0.6.0.tar.gz'

    maintainers = ['paulapopa', 'ldomide']

    version('0.6.0', '36392049ec5a2ccae75a0cf89b21838fda41a351c31fe6c6622cfcff1c997933')

    # python_requires
    depends_on('python@3.8:', type=('build', 'run'))

    # setup_requires
    depends_on('py-pip', type=('build'))
    depends_on('py-setuptools', type='build')
    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-duecredit', type=('build', 'run'))

    depends_on('py-pytest', type='test')

