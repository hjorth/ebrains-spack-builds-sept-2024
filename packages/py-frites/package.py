# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyFrites(PythonPackage):
    """Frites is a Python toolbox for assessing information-based measures on
    human and animal neurophysiological data (M/EEG, Intracranial)."""

    homepage = "https://brainets.github.io/frites/"
    pypi     = "frites/frites-0.4.2.tar.gz"

    maintainers = ['ecombrisson']

    version('0.4.4', sha256='280ca9f3f711da0550eceec8ce94108b891369b63ea3cc1b8c960825ed20fb3f')
    version('0.4.3', sha256='41a0aa2414196f1a08b7668b3e6be5872eda0f6814171408ef33af4904619f3a')
    version('0.4.2', sha256='5da63f6f91515ff0dbed0252a75fcbdeade4311bfd28d08cd644c89c85e6ac5a')

    depends_on('python@3.7:', type=('build', 'run'))

    depends_on('py-setuptools', type='build')

    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-mne', type=('build', 'run'))
    depends_on('py-mne@:1.1.1', type=('build', 'run'), when='@:0.4.2')
    depends_on('py-joblib', type=('build', 'run'))
    depends_on('py-xarray', type=('build', 'run'))
    depends_on('py-netcdf4', type=('build', 'run'))
    depends_on('py-h5netcdf', type=('build', 'run'))
    depends_on('py-scikit-learn', type=('build', 'run'))
    depends_on('py-decorator', type=('build', 'run'))
