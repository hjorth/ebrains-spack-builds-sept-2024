# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyHbpValidationClient(PythonPackage):
    """Python client for the HBP Validation Framework web services"""
    # This is a wrapper for py-ebrains-validation-client, for backwards compatibility

    homepage = "https://hbp-validation-client.readthedocs.io/"
    pypi     = "hbp_validation_framework/hbp_validation_framework-0.8.1.tar.gz"
    git      = "https://github.com/HumanBrainProject/hbp-validation-client"

    maintainers = ["apdavison"]

    version("0.9.0", sha256="39925124eb4738190537179c275e79b23110854c7774f26d028302526433fa36")
    version("0.8.3", sha256="f4feb6fa57f1f7583c3c8a8cf593d257e6ea750f32086f9fd6646af3fda29010")
    version("0.8.2", sha256="ba1cf2ecfc10de35e106884d2734faea069b939830990989f9ae08e43d6fde64")
    version("0.8.1", sha256="2afa96c07a5646e3e37528bacf103da4b352353ac45c10bf5948e4efbbbfb2a0")
    version("0.8.0", sha256="948fed2c83230cfab07416763b3d0e8adb0de1ee4c3a1a911697045025ff540f")
    version("0.7.1", sha256="bf431c952e4d4c3f63ec7165236baba3d412fb5ea7240ff6d26e3254d3234ff5")
    version("0.7.0", sha256="0ec698019bc874f013de845ddef153d15f0460f251ef7a5d10bc2a1c464d1bc5")
    version("0.6.6", sha256="00e48eea922f08d711e61dae0194602dfc4456c2ed29c93aa01b0c992b70b88b")
    version("0.6.4", sha256="d011c53744ab1ce159ac2cf702851d2ad6e763fef67b192c02ac3ab1fe47c231")
    version("0.6.3", sha256="e9d27a5d1bbc7066877d6a97eb1bcb90c70b5ec00d9027425d85f796c6a9e8c4")

    depends_on('python@3.6.9:')
    depends_on('py-setuptools',            type=('build'))
    depends_on('py-requests@2.26.0:',      type=('build', 'run'), when='@:0.8.4')
    depends_on('py-nameparser@1.1.1:',     type=('build', 'run'), when='@:0.8.4')
    depends_on('py-ebrains-drive@0.4.0:',  type=('build', 'run'), when='@:0.8.4')
    depends_on('py-simplejson@3.17.2:',    type=('build', 'run'), when='@:0.8.4')
    depends_on('py-sciunit',               type=('build', 'run'))
    depends_on('py-ebrains-validation-client@0.9.1:',   type=('build', 'run'), when='@0.9.0:')
