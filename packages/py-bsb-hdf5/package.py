# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyBsbHdf5(PythonPackage):
    """An HDF-5 based storage engine for the BSB framework."""

    homepage = "https://github.com/dbbs-lab/bsb-hdf5"
    pypi = "bsb-hdf5/bsb_hdf5-5.0.4.tar.gz"

    license("GPL-3.0-only")
    maintainers = ["helveg","filimarc","drodarie"]

    version('5.0.2', sha256='ed11177887848a3f177982201e1adb5770131bd541055a96935af38b39439fac')

    depends_on("py-flit-core@3.2:4.0", type="build")
    depends_on("py-bsb-core@5.0.0:",when='@5.0.2')
    depends_on("py-shortuuid")
