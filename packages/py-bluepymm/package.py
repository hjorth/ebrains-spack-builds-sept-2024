# (from https://github.com/BlueBrain/spack)

# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyBluepymm(PythonPackage):
    """Blue Brain Model Management Python Library"""

    homepage = "https://github.com/BlueBrain/BluePyMM"
    pypi = "bluepymm/bluepymm-0.7.49.tar.gz"

    version("0.8.7", sha256="f0e5d4e113b19f71398d0796d5182f322c48c2ab07793ce8d0e4771a251914ab")
    version("0.7.65", sha256="024b009decd8d967b3b885421196d53670e3c0a6b75aaaa55559f148b0b0d7d4")

    depends_on("py-setuptools", type="build")
    depends_on("py-bluepyopt", type=("run","test"))
    depends_on("py-matplotlib", type=("run","test"))
    depends_on("py-pandas", type=("run","test"))
    depends_on("py-numpy", type=("run","test"))
    depends_on("py-ipyparallel", type=("run","test"))
    depends_on("py-lxml", type=("run","test"))
    depends_on("py-sh", type=("run","test"))
    depends_on("neuron", type=("run","test"))
    depends_on("py-h5py", type=("run","test"))
    depends_on("py-pyyaml", type=("run","test"))

    def setup_run_environment(self, env):
        env.unset("PMI_RANK")
        env.set("NEURON_INIT_MPI", "0")
