# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyPymc(PythonPackage):
    """PyMC (formerly PyMC3) is a Python package for Bayesian statistical modeling focusing on advanced Markov chain Monte
    Carlo (MCMC) and variational inference (VI) algorithms."""

    homepage = "https://github.com/pymc-devs/pymc"
    pypi = "pymc/pymc-5.20.1.tar.gz"

    version("5.20.1", "fb5f20d196a1b34eb193a855c611887b2e7b98d3af37d8573a33d112e2278eac")

    depends_on("python@3.10:", type=("build", "run"))
    depends_on("py-setuptools", type="build")
    depends_on("py-versioneer", type="build")

    depends_on("py-arviz@0.13:", type=("build", "run"))
    depends_on("py-cachetools@4.2.1:", type=("build", "run"))
    depends_on("py-cloudpickle", type=("build", "run"))
    depends_on("py-numpy@1.25.0:", type=("build", "run"))
    depends_on("py-pandas@0.24.0:", type=("build", "run"))
    depends_on("py-pytensor@2.26.1:2.27.999", type=("build", "run"))
    depends_on("py-rich@13.7.1:", type=("build", "run"))
    depends_on("py-scipy@1.4.1:", type=("build", "run"))
    depends_on("py-threadpoolctl@3.1.0:3.99", type=("build", "run"))
    depends_on("py-typing-extensions@3.7.4:", type=("build", "run"))
