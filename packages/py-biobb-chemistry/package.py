# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *

class PyBiobbChemistry(PythonPackage):
    """Biobb_chemistry is the Biobb module collection to perform chemistry 
    over molecular dynamics simulations."""

    # Homepage and download url
    homepage = "https://github.com/bioexcel/biobb_chemistry"
    git = 'https://github.com/bioexcel/biobb_chemistry.git'
    url = 'https://github.com/bioexcel/biobb_chemistry/archive/refs/tags/v4.1.0.tar.gz'

    # Set the gitlab accounts of this package maintainers
    maintainers = ['dbeltran']

    # Versions
    version('4.1.0', sha256='8d04943bbfcd83eb4d5d3247949cf035977fd097d5014a39354b0502e1cd16c8')

    # Dependencies
    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('py-biobb-common')
    depends_on('openbabel')
    depends_on('ambertools')
    depends_on('acpype')

    # Test
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install (self):
        python("-c", 'import biobb_chemistry')
