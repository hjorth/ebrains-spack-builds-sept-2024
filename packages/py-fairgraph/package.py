# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyFairgraph(PythonPackage):
    """High-level Python API for the EBRAINS Knowledge Graph."""

    homepage = "https://github.com/HumanBrainProject/fairgraph"
    pypi = "fairgraph/fairgraph-0.8.1.tar.gz"

    maintainers = ["apdavison"]

    version("0.12.1", sha256="b94429d253ee4c80133ce655794552f5dd7a48ebfad130801a5e1297e6e0cee5")
    version("0.12.0", sha256="b0875c55a8fce13bcffe37c8d229b27b63a30ede2d3c39e8f55ba5033ad8a3fe")
    version("0.11.1", sha256="e4bc1737af032e7a9f693c8f12112e100762dd9597022aed012ae05d8196ebeb")
    version("0.11.0", sha256="ae87acf0d9f7e0c296a889056855e759705132f2fb88f77b08c708528f9823bb")
    version("0.10.0", sha256="4ad387549a12886323f149867c0940d2e3855f2fd9b3fb44af2ba312cc4975b7")
    version("0.9.0", sha256="8ed3dd3bc274d9c25408a2e971f317d4e3aa4400636e9f4b89716ad92b721d78")
    version("0.8.2", sha256="2788acacfcab4bb2c256ddd46ca961c94bd2ca15163f1e03dcd16adbbc3784f7")
    version("0.8.1", sha256="2d2b3c01cd461b0dcddd5cbe3903d59f67d4a1d2f5fee92b6e25281b087a3e81")

    depends_on("py-setuptools", type="build")

    depends_on("py-ebrains-kg-core", type=("build", "run"))
    depends_on("py-pyld@0.8.2", type=("build", "run"), when='@:0.9.0')
    depends_on("py-pathlib2", type=("build", "run"), when='@:0.8.2')
    depends_on("py-python-dateutil", type=("build", "run"))
    depends_on("py-six", type=("build", "run"), when='@:0.8.2')
    depends_on("py-tabulate", type=("build", "run"))
    depends_on("py-tqdm", type=("build", "run"), when='@:0.9.0')
