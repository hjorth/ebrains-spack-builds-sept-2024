# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)


from spack.package import *


class PyDashing(PythonPackage):
    """Dashing is a library to quickly create terminal-based dashboards in Python."""

    homepage = "https://github.com/FedericoCeratto/dashing"
    pypi = "dashing-next/dashing_next-0.1.0.tar.gz"

    version("0.1.0", sha256="9d48e97fce430a9cfb47d5627041b001ab306b65e97d6967fe86e2c25e324612")

    depends_on("py-flit-core@3.2:4.0", type="build")
    depends_on("py-blessed@1.20.0:")
