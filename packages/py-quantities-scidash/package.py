# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyQuantitiesScidash(PythonPackage):
    """Support for physical quantities with units, based on numpy
    """

    homepage = "http://python-quantities.readthedocs.io/"
    pypi     = "quantities-scidash/quantities-scidash-0.12.4.3.tar.gz"
    git      = "https://github.com/python-quantities/python-quantities.git"

    maintainers = ["apdavison"]

    version('0.12.4.3', sha256='c281383b989ae19bfe1526c09e452c82880b633a735f9494d8bb469412a33ea2')

    depends_on('python@3.6.9:')
    depends_on('py-setuptools', type=('build'))
    depends_on('py-numpy@1.19:')
