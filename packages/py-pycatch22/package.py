# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyPycatch22(PythonPackage):
    
    homepage = "https://github.com/DynamicsAndNeuralSystems/pycatch22"
    pypi="pycatch22/pycatch22-0.4.5.tar.gz"
    
    version("0.4.5", sha256="7ec844c659f22bedc66847ac866ef2bd86ffbbd4d8114b5e97f699f20a6f9f81")

    depends_on("py-setuptools", type="build")
