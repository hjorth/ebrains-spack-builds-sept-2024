# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyMmcifPdbx(PythonPackage):
    """
    This is yet another PyPI package for
    http://mmcif.wwpdb.org/dbx-mmcif-home-page.html.
    It emphasizes a simple and pure Python interface
    to basic mmCIF functionality.
    """

    # Url for the package's homepage.
    homepage = "https://mmcif-pdbx.readthedocs.io/"
    pypi     = "mmcif-pdbx/mmcif-pdbx-2.0.1.tar.gz"

    # List of GitHub accounts to
    # notify when the package is updated.
    maintainers = ['richtesn', 'thielblz']

    version('2.0.1', sha256='a1f20cf35f92916160ae066f510cce9a0c21a630d064dafce545ba0cf47c9280')

    depends_on('py-setuptools', type=('build'))
