# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyAnnarchy(PythonPackage):
    """
    ANNarchy (Artificial Neural Networks architect) is a neural simulator designed
    for distributed rate-coded or spiking neural networks.
    """

    homepage = "https://annarchy.readthedocs.io/en/latest/"
    pypi = 'ANNarchy/annarchy-4.8.2.3.tar.gz'

    maintainers = ['dionperd', 'paulapopa', "ldomide"]

    version('4.8.2.3', '25a4d09905983ce27f7c6b4dd67a54831ea233b6b28943cb67dafd3c351d1dde')
    version('4.7.2.5', 'b7ef91cc4415e078e386eb30e595922c9f0ef90ad1340a12dc5ca46e728a7bb2')

    # python_requires
    depends_on('python@3.8:3.10', when='@:4.7.2', type=('build', 'run'))
    depends_on('python@3.10:', when='@4.7.3:', type=('build', 'run'))

    # setup_requires
    depends_on('py-pip', type='build')
    depends_on('py-setuptools', type='build')  # >= 40.0

    # install_requires
    # TODO: Find package for g++ >= 6.1 ( >= 7.4 recommended )
    depends_on("cmake", type=('build', 'run'))  # TODO: Find out if this will install make >= 3.0
    depends_on('py-numpy', type=('build', 'run'))  # >= 1.13
    depends_on('py-scipy', type=('build', 'run'))  # >= 0.19
    depends_on('py-sympy', type=('build', 'run'))  # >= 1.6
    depends_on('py-matplotlib', type=('build', 'run'))  # >= 2.0
    depends_on('py-cython', type=('build', 'run'))  # >= 0.20
    depends_on('py-tqdm', when='@4.8:', type=('build', 'run'))
    depends_on('py-h5py', when='@4.8.2:', type=('build', 'run'))

    # Highly recommended:
    # pyqtgraph >= 0.9.8 (to visualize some of the provided examples. The OpenGL backend can also be needed)
    depends_on('py-lxml', type=('build', 'run'))  # lxml >= 3.0 (to save the networks in .xml format)
    # pandoc >= 2.0 (for reporting):
    # https://github.com/spack/spack/blob/develop/var/spack/repos/builtin/packages/pango/package.py
    depends_on('pandoc', type=('build', 'run'))
    # tensorboardX (for the logging extension):
    # https://github.com/spack/spack/blob/develop/var/spack/repos/builtin/packages/py-tensorboardx/package.py
    depends_on('py-tensorboardx', type=('build', 'run'))

    # Test dependency
    depends_on('py-pytest@:7.1', type='test')

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which('pytest')
        pytest()
