# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyHippounit(PythonPackage):
    """A SciUnit library for data-driven validation testing of models of hippocampus
    """

    homepage = "https://github.com/kalilab/hippounit/"
    pypi     = "hippounit/hippounit-1.3.6.tar.gz"
    git      = "https://github.com/kalilab/hippounit.git"

    version('1.4.1', sha256='2cd8cac4ef4403b2eca098ba1f786183a3556571906c78fe8b5081b2040ecb83')
    version('1.3.6', sha256='7342f77575b6d223db4194be37ab73f0eb6370844cbefd2f8374b2c7e27d7d15')

    depends_on('python@3.6.9:')
    depends_on('py-setuptools',       type=('build'))
    depends_on('py-future',           type=('build', 'run'))
    depends_on('py-scipy',            type=('build', 'run'))
    depends_on('py-sciunit@0.2.5.1:', type=('build', 'run'))
    depends_on('py-efel@4.0.4:',      type=('build', 'run'))
