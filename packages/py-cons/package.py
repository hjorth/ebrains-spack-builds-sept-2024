# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyCons(PythonPackage):
    """An implementation of cons in Python."""

    homepage = "https://github.com/pythological/python-cons"
    pypi = "cons/cons-0.4.6.tar.gz"

    version("0.4.6", "669fe9d5ee916d5e42b9cac6acc911df803d04f2e945c1604982a04d27a29b47")

    depends_on("python@3.6:", type=("build", "run"))
    depends_on("py-setuptools", type="build")
    depends_on("py-logical-unification@0.4.0:", type=("build", "run"))
