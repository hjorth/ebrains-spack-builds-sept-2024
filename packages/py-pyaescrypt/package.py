# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyPyaescrypt(PythonPackage):
    """
    Requirement necessary for py-tvb-framework package.
    """

    homepage = "https://pypi.org/project/pyAesCrypt"
    pypi = 'pyAesCrypt/pyAesCrypt-6.1.1.tar.gz'

    maintainers = ['paulapopa', 'ldomide']

    version('6.1.1', '6bf8f97c03ec0e42008da911ac25e523f11f160f684d5f2bc9579ce501be9eae')
    version('6.0.0', 'a26731960fb24b80bd3c77dbff781cab20e77715906699837f73c9fcb2f44a57')

    # python_requires
    depends_on('python@3.8:', type=('build', 'run'))

    # setup_requires
    depends_on('py-setuptools', type='build')
    depends_on('py-cryptography', type='run')
