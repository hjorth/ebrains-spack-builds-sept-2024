# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

import os

from llnl.util import filesystem

from spack.package import *


class Open3d(CMakePackage, CudaPackage):
    """Open3D: A Modern Library for 3D Data Processing."""

    homepage = "https://www.open3d.org/"
    url = "https://github.com/isl-org/Open3D/archive/refs/tags/v0.13.0.tar.gz"
    git = "https://github.com/isl-org/Open3D.git"

    license("MIT")

    # begin EBRAINS (added): add version
    version(
        "0.19.0", tag="v0.19.0", commit="1e7b17438687a0b0c1e5a7187321ac7044afe275", submodules=True
    )
    version(
        "0.18.0", tag="v0.18.0", commit="0f06a149c4fb9406fd3e432a5cb0c024f38e2f0e", submodules=True
    )
    # end EBRAINS
    version(
        "0.13.0", tag="v0.13.0", commit="c3f9de224e13838a72da0e5565a7ba51038b0f11", submodules=True
    )

    depends_on("c", type="build")  # generated
    depends_on("cxx", type="build")  # generated
    depends_on("fortran", type="build")  # generated

    variant("python", default=False, description="Build the Python module")

    # http://www.open3d.org/docs/latest/compilation.html

    depends_on("cmake@3.19:", type="build")
    # https://github.com/isl-org/Open3D/issues/3762
    # https://github.com/isl-org/Open3D/issues/4570
    depends_on("llvm@7:+clang")
    # begin EBRAINS (modified)
    depends_on("eigen", type=("build"))
    depends_on("flann", when="@:0.13")
    # https://github.com/isl-org/Open3D/issues/4360
    # https://github.com/isl-org/Open3D/pull/5303
    depends_on("fmt@:7", when="@:0.16")
    depends_on("fmt@:9", when="@0.17:")
    depends_on("googletest")
    depends_on("glew")
    depends_on("glfw")
    # depends_on('imgui')
    depends_on("jpeg")
    # depends_on('liblzf')
    # depends_on("libpng")
    depends_on("nanoflann@:1.4", when="@0.15:0.17")
    depends_on("nanoflann@1.5:", when="@0.18:")
    depends_on("py-pybind11", type=("build"))
    # end EBRAINS
    depends_on("qhull")
    # depends_on('tinygltf')
    # depends_on('tinyobjloader')

    # begin EBRAINS (added)
    depends_on("assimp", when="@0.15:")
    depends_on("jsoncpp", when="@0.15:")
    depends_on("msgpack-c", when="@0.15:")
    depends_on("tbb", when="@0.15:")
    depends_on("cppzmq", when="@0.15:")
    depends_on("curl", when="@0.17:")
    depends_on("openssl", when="@0.17:")
    # depends_on("vtk", when="@0.17:")
    depends_on("embree@:3", when="@0.18")
    depends_on("embree@4:", when="@0.19:")
    # end EBRAINS

    extends("python", when="+python", type=("build", "link", "run"))
    depends_on("python@3.6:", when="+python", type=("build", "link", "run"))
    depends_on("py-pip", when="+python", type="build")
    depends_on("py-setuptools@40.8:", when="+python", type="build")
    depends_on("py-wheel@0.36:", when="+python", type="build")
    depends_on("py-numpy@1.18:", when="+python", type=("build", "run"))
    # begin EBRAINS (added): add python dependencies
    depends_on("py-dash@2.6.0:", when="+python", type=("build", "run"))
    depends_on("py-nbformat@5.7.0:", when="+python", type=("build", "run"))
    depends_on("py-werkzeug@2.2.3:", when="+python", type=("build", "run"))
    depends_on("py-configargparse", when="+python", type=("build", "run"))
    # end EBRAINS
    depends_on("py-pytest", when="+python", type="test")
    depends_on("cuda@10.1:", when="+cuda")

    # C++14 compiler required
    conflicts("%gcc@:4")
    conflicts("%clang@:6")

    # LLVM must be built with the C++ library
    conflicts("^llvm libcxx=none")

    # begin EBRAINS (added)
    patch("glew-no-glu.patch")
    # end EBRAINS

    def patch(self):
        # Force Python libraries to be installed to self.prefix
        # begin EBRAINS (modified)
        args = std_pip_args + ["--prefix=" + self.prefix]
        filter_file(
            "pip install",
            "pip " + " ".join(args),
            os.path.join("cpp", "pybind", "make_install_pip_package.cmake"),
        )
        # end EBRAINS

    def cmake_args(self):
        # begin EBRAINS (modified)
        libcxx_paths = filesystem.find_libraries(
            "libc++", self.spec["llvm"].prefix, shared=True, recursive=True
        )
        if len(libcxx_paths) > 1:
            raise InstallError(
                "concretized llvm dependency must provide a "
                "unique directory containing libc++.so, "
                "found: {0}".format(libcxx_paths)
            )
        args = [
            self.define("BUILD_UNIT_TESTS", self.run_tests),
            self.define_from_variant("BUILD_PYTHON_MODULE", "python"),
            self.define_from_variant("BUILD_CUDA_MODULE", "cuda"),
            # https://github.com/isl-org/Open3D/issues/4570
            # self.define('BUILD_FILAMENT_FROM_SOURCE', 'ON'),
            # Use Spack-installed dependencies instead of vendored dependencies
            # Numerous issues with using externally installed dependencies:
            # https://github.com/isl-org/Open3D/issues/4333
            # https://github.com/isl-org/Open3D/issues/4360
            self.define("USE_SYSTEM_EIGEN3", True),
            self.define("USE_SYSTEM_FMT", True),
            self.define("USE_SYSTEM_GOOGLETEST", True),
            self.define("USE_SYSTEM_GLEW", True),
            self.define("USE_SYSTEM_GLFW", True),
            # self.define('USE_SYSTEM_IMGUI', True),
            self.define("USE_SYSTEM_JPEG", True),
            # self.define('USE_SYSTEM_LIBLZF', True),
            # https://github.com/isl-org/Open3D/issues/4883
            # self.define("USE_SYSTEM_PNG", True),
            self.define("USE_SYSTEM_PYBIND11", True),
            # self.define('USE_SYSTEM_TINYGLTF', True),
            # self.define('USE_SYSTEM_TINYOBJLOADER', True),
            self.define("CLANG_LIBDIR", os.path.dirname(libcxx_paths[0])),
        ]
        if self.spec.satisfies("@:0.13"):
            args.append(self.define("USE_SYSTEM_FLANN", True))
            args.append(self.define("USE_SYSTEM_QHULL", True))
        if self.spec.satisfies("@0.14:"):
            args.append(self.define("USE_SYSTEM_QHULLCPP", True))
        if self.spec.satisfies("@0.15:"):
            args.append(self.define("USE_SYSTEM_ASSIMP", True))
            args.append(self.define("USE_SYSTEM_JSONCPP", True))
            args.append(self.define("USE_SYSTEM_MSGPACK", True))
            args.append(self.define("USE_SYSTEM_NANOFLANN", True))
            args.append(self.define("USE_SYSTEM_TBB", True))
            args.append(self.define("USE_SYSTEM_ZEROMQ", True))
        if self.spec.satisfies("@0.17:"):
            args.append(self.define("USE_SYSTEM_CURL", True))
            args.append(self.define("USE_SYSTEM_OPENSSL", True))
            # args.append(self.define("USE_SYSTEM_VTK", True))
        if self.spec.satisfies("@0.18:"):
           args.append(self.define("USE_SYSTEM_EMBREE", True))
        # cf. https://github.com/spack/spack/issues/42839
        args.append(self.define("DEFINE_GLEW_NO_GLU", True))
        # end EBRAINS

        return args

    def check(self):
        # begin EBRAINS (modified)
        with working_dir(self.builder.build_directory):
        # end EBRAINS
            tests = Executable(os.path.join("bin", "tests"))
            tests()

    def install(self, spec, prefix):
        # begin EBRAINS (modified)
        with working_dir(self.builder.build_directory):
        # end EBRAINS
            make("install")
            if "+python" in spec:
                make("install-pip-package")

    # Tests don't pass unless all optional features are compiled, including PyTorch
    # @run_after('install')
    # @on_package_attributes(run_tests=True)
    # def unit_test(self):
    #     if '+python' in self.spec:
    #         pytest = which('pytest')
    #         pytest(os.path.join('python', 'test'))

    @run_after("install")
    @on_package_attributes(run_tests=True)
    def test_open3d_import(self):
        """Checking import of open3d"""
        if "+python" not in self.spec:
            return

        with working_dir("spack-test"):
            python = which(python.path)
            python("-c", "import open3d")

