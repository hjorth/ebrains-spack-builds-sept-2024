# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class WfMultiAreaModel(Package):
    """Meta-package to collect all dependencies of the Multi-Area-Model."""

    homepage="https://inm-6.github.io/multi-area-model/"
    git = "https://github.com/INM-6/multi-area-model"
    maintainer = ["terhorstd", "didi-hou", "rshimoura"]

    version("1.2.0",  tag="v1.2.0")
    version("1.1.1",  tag="v1.1.1")
    version("1.1.0",  tag="v1.1.0")
    version("master",  branch="master")

    depends_on("py-nested-dict", type=("run", "test"))
    depends_on("py-dicthash", type=("run", "test"))
    depends_on("py-matplotlib", type=("run", "test"))
    depends_on("py-numpy", type=("run", "test"))
    depends_on("py-scipy", type=("run", "test"))
    depends_on("py-future", type=("run", "test"))
    depends_on("nest", type=("run", "test"))
    depends_on("py-neo", type=("run", "test"))
    depends_on("py-elephant", type=("run", "test"))
    depends_on("r-aod", type=("run", "test"))
    depends_on("py-notebook", type=("run", "test"))

    def install(self, spec, prefix):
        install_tree(".", join_path(prefix, "notebooks"))

    def _nbconvert(self, nb, nb_out):
        jupyter = Executable("jupyter")
        args = [
            "nbconvert",
            "--ExecutePreprocessor.kernel_name=python3",
            "--execute",
            "--to",
            "notebook",
            nb,
            "--output",
            nb_out
        ]
        try:
            # execute notebook and save
            jupyter(*args)
        except Exception as e:
            # if the above fails, re-run notebook to produce output with error
            jupyter(*(args+["--allow-errors"]))
            raise

    def _run_notebooks(self, output_dir):
        mkdirp(output_dir)
        self._nbconvert(join_path(self.prefix, "notebooks", "multi-area-model.ipynb"), join_path(output_dir, "multi-area-model.ipynb"))

    @run_after("install")
    @on_package_attributes(run_tests=True)
    def installcheck(self):
        self._run_notebooks(join_path(self.stage.path, ".install_time_tests"))
        copy_tree(join_path(self.stage.path, ".install_time_tests"), join_path(self.prefix, '.build'))

    def test_notebook(self):
        self._run_notebooks(join_path(self.test_suite.stage, self.spec.format("out-{name}-{version}-{hash:7}")))
