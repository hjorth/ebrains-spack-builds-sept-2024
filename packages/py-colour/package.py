# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyColour(PythonPackage):
    """Python color representations manipulation library (RGB, HSL, web, ...)"""

    homepage = "https://github.com/vaab/colour"
    pypi = "colour/colour-0.1.5.tar.gz"

    maintainers = ["vaab"]

    version("0.1.5", sha256="af20120fefd2afede8b001fbef2ea9da70ad7d49fafdb6489025dae8745c3aee")
    depends_on("py-setuptools", type="build")
