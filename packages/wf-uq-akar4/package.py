# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class WfUqAkar4(BundlePackage):
    """Meta-package to collect all dependencies of the Uncertainty Quantification demo."""

    homepage="https://wiki.ebrains.eu/bin/view/Collabs/subcellular-modeling-and-simulation/Lab"

    maintainers = ["akramer", "oliviaeriksson"]

    version("0.1")

    depends_on("r-mass")
    depends_on("r-desolve")
    depends_on("r-pracma")
    depends_on("r-ks")
    depends_on("r-reshape2")
    depends_on("r-r-utils")
    depends_on("r-vinecopula")
    depends_on("r-ggplot2")
