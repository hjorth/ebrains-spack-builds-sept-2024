# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyJaracoText(PythonPackage):
    """Module for text manipulation"""

    homepage = "https://github.com/jaraco/jaraco.text"
    pypi = "jaraco.text/jaraco.text-3.12.0.tar.gz"

    license("MIT")

    version("3.12.0", sha256="389e25c8d4b32e9715bf530596fab0f5cd3aa47296e43969392e18a541af592c")
    version("3.11.1", sha256="333a5df2148f7139718607cdf352fe1d95162971a7299c380dcc24dab0168980")
    version("3.11.0", sha256="0ddb589595fe176ea8179c801ca4ece2be0aa71f377b91f3ca65b4d741948351")

    depends_on("python@3.8:", type=("build", "run"))

    depends_on("py-setuptools@56:", type="build")
    depends_on("py-setuptools-scm@3.4.1: +toml", type="build")

    depends_on("py-jaraco-functools", type=("build", "run"))
    depends_on("py-jaraco-context@4.1:", type=("build", "run"))
    depends_on("py-importlib-resources@1.3:", when="^python@:3.9", type=("build", "run"))
    depends_on("py-inflect", type=("build", "run"))
    depends_on("py-more-itertools", type=("build", "run"))
