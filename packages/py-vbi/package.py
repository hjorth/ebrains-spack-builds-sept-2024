# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyVbi(PythonPackage, CudaPackage):

    homepage = "https://vbi.readthedocs.io/latest/"
    git = "https://github.com/ins-amu/vbi"
    url = "https://github.com/ins-amu/vbi/archive/refs/tags/v0.1.3.tar.gz"

    version("0.1.3", "8ccccf2bf0def2bf97f4706b8597c4cb3ac5f0cf2ac5f08566e22cd6273c1163")
    version("0.1.2", "6ccfeeec718be62a480002a8370130a3e3344955186f99ecbb15b646b68210d6")
    


    depends_on("python@3.8:", type=("build","run"))
    depends_on("py-setuptools", type="build")
    depends_on("py-setuptools-scm", type="build")
    depends_on("py-wheel", type="build")
    depends_on("swig@4:", type="build")
    depends_on("py-numpy", type=("build", "run"))
    depends_on("py-scipy", type=("build", "run"))
    depends_on("py-numba", type=("build", "run"))
    depends_on("py-h5py", type=("build", "run"))
    depends_on("py-pandas", type=("build", "run"))
    depends_on("py-networkx", type=("build", "run"))
    depends_on("py-nbconvert", type=("build", "run"))
    depends_on("py-matplotlib", type=("build", "run"))
    depends_on("py-tqdm", type=("build", "run"))
    depends_on("py-sbi", type=("build", "run"))
    depends_on("py-torch", type=("build", "run"))
    depends_on("py-parameterized", type=("build", "run"))
    depends_on("py-scikit-learn", type=("build", "run"))
    depends_on("py-pycatch22", type=("build", "run"))
    depends_on("py-pytest", type="test")
    depends_on("py-cupy", type=("build", "run"), when="+cuda")
    
    @run_after("install")
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which("pytest")
        pytest()
