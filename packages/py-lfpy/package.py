# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyLfpy(PythonPackage):
    """A Python package for calculating extracellular electric and magnetic
    signals of neural activity from multicompartment neuron models.
    """

    homepage = 'https://LFPy.readthedocs.io'
    pypi = "LFPy/LFPy-2.3.tar.gz"
    git = 'https://github.com/LFPy/LFPy.git'

    maintainers = ['espenhgn', 'ness']

    version('2.2.6', sha256='105046db3628c6f72a6ab96ee8ac0f4833bf8eee428dcee50cb7f2926b755966')
    version('2.3', sha256='29b2551fcd0391dcc5fa378750e2fcf58bec68f75568b590823c9b105da0c97e')

    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('py-pip', type='build')
    depends_on('py-h5py', type=('build', 'run'))
    depends_on('py-matplotlib', type=('build', 'run'))
    depends_on('py-mpi4py', type=('build', 'run'))
    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-setuptools', type=('build', 'run'))
    depends_on('py-cython', type=('build', 'run'))
    depends_on('neuron@7.7.2:', type=('build', 'run'))
    depends_on('py-lfpykit@0.5:', type=('build', 'run'))
    depends_on('py-pytest', type='test')
    

    # Code to run post installation tests    
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self): 
        with working_dir('spack-test', create=True):
            python('-c', 'import LFPy; LFPy.run_tests()')

