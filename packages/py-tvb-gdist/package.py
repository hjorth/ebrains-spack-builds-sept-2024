# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyTvbGdist(PythonPackage):
    """
    "The Virtual Brain" Project (TVB Project) has the purpose of offering modern tools to the Neurosciences community,
    for computing, simulating and analyzing functional and structural data of human brains, brains modeled at the level
    of population of neurons.
    """

    homepage = "https://www.thevirtualbrain.org/"
    pypi = 'tvb-gdist/tvb-gdist-2.2.tar.gz'

    maintainers = ['paulapopa', 'ldomide']

    version('2.2', '724bffbf985936bf474b27c33fcc9d764477e4f153edcaf6574dcc5bfa32ef33')
    version('2.1.0', '802fcc65d8007aaa14c0f6e6aded419fb38c27995ac94a2f945499b1f2218fd8')

    # python_requires
    depends_on('python@3.8:3.11', type=('build', 'run'))

    # setup_requires
    depends_on('py-setuptools', type='build')
    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-cython', type=('build', 'run'))

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        # TODO tests are not packed on Pypi from tvb-gdist repo
        with working_dir('spack-test', create=True):
            python('-c', 'import gdist')


