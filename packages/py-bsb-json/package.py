# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyBsbJson(PythonPackage):
    """A plugin that allows the user to write their models' configuration in the json format, for the BSB framework."""

    homepage = "https://github.com/dbbs-lab/bsb-json"
    pypi = "bsb-json/bsb_json-4.2.2.tar.gz"

    license("GPL-3.0-only")
    maintainers = ["helveg","filimarc","drodarie"]

    version("4.2.2", sha256="0c9e0af2a50f8ebbce353ba19bd11bafaf2536d74f0a79af3b0b6d8241fa6937")

    depends_on("py-flit-core@3.2:4.0", type="build")
    depends_on("py-bsb-core@5.0.0:")
    depends_on("py-shortuuid")
