# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class WfProteinAssociationRates(BundlePackage):
    """Meta-package to collect all dependencies for the "Molecular Tools: protein
    association rates and binding sites" demos/workflows."""

    homepage="https://wiki.ebrains.eu/bin/view/Collabs/computation-of-protein-association-rates/"

    maintainers = ['thielblz', 'richtesn']

    version("0.1")

    depends_on("apbs")
    depends_on("py-pdb2pqr")
