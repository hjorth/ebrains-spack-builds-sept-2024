# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class WfCustomPython(BundlePackage):
    """Meta-package to represent a use case where users install additional
    (on top of the ESD) Python packages."""

    version("0.1")
    
    depends_on("py-pip")
    depends_on("py-virtualenv")
