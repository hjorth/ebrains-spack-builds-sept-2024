# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PySnudda(PythonPackage):
    """Snudda creates the connectivity for realistic networks of simulated neurons in silico in a bottom up fashion that can then be simulated using the NEURON software."""

    homepage = "https://pypi.org/project/snudda/"
    pypi     = "snudda/snudda-2.1.2.tar.gz"

    maintainers = ["hjorth"]
    
    version("2.1.2", "5d61a548995f88f95f680bf124534287")
    version("2.0.1", "0d78f5ca2cfe728b216f980078d8558a")
    version("1.4.71", "5871e4af5e1a011d26a22d7dc384638a")
    version("1.4.0", "55f9b398b01b34bf3cec28c8a3aebc78")
    version("1.3.2", "2306ec50acead5fd4f988ec373f19718", url="https://files.pythonhosted.org/packages/py3/s/snudda/snudda-1.2.9-py3-none-any.whl", expand=False)

    depends_on("unzip",                 type=("build"))
    depends_on("py-setuptools",         type=("build"))
    depends_on("py-importlib-metadata", type=("build","run"))
    depends_on("py-bluepyopt@1.14.15",  type=("build","run"))
    depends_on("py-h5py@3.12.1",        type=("build","run"))
    depends_on("py-ipyparallel@8.8.0", type=("build","run"))
    depends_on("py-matplotlib@3.9.2",  type=("build","run"))
    depends_on("py-mpi4py@4.0.0",      type=("build","run"))
    depends_on("py-numpy@1.26.4",      type=("build","run"))
    depends_on("py-scipy@1.13.1",       type=("build","run"))
    depends_on("py-libsonata@0.0.2",   type=("build","run"))
    depends_on("py-pyzmq@26.2",      type=("build","run"))
    depends_on("py-numexpr@2.10.1",     type=("build","run"))
    depends_on("neuron@8.2.6",         type=("build","run"))
    depends_on("py-pyswarms@1.3.0",    type=("build","run"))
    depends_on("py-psutil",             type=("build","run"))
    depends_on("py-cython",             type=("build","run"))
    depends_on("py-numba@0.60.0:",      type=("build","run"))
    depends_on("open3d+python",         type=("build","run"), when="@2:")


    # snudda tarballs in pypi do not include the tests/ dir: just use default spack tests for now
    # @run_after('install')
    # @on_package_attributes(run_tests=True)
    # def install_test(self):
    #     python('-m', 'unittest', 'discover', '-v', '-s' './tests')

    # blender is for now an optional dependency: leave out of import_modules to avoid errors in tests
    skip_modules = ['snudda.plotting.Blender', 'snudda.plotting.Blender.io_mesh_swc', 'snudda.plotting.Blender.visualisation']
