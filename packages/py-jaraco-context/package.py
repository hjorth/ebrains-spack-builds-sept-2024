# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyJaracoContext(PythonPackage):
    """Useful decorators and context managers"""

    homepage = "https://github.com/jaraco/jaraco.context"
    pypi = "jaraco.context/jaraco.context-5.1.0.tar.gz"

    license("MIT")

    version("5.1.0", sha256="24ec1f739aec2c5766c68027ccc70d91d7b0cb931699442f5c7ed93515b955e7")
    version("5.0.0", sha256="e0e3a7e5ce2dc17daf5f7a0e9387eebb8f352514fd43418ced34bddc6063c34f")
    version("4.3.0", sha256="4dad2404540b936a20acedec53355bdaea223acb88fd329fa6de9261c941566e")

    depends_on("python@3.8:", type=("build", "run"))

    depends_on("py-setuptools@56:", type="build")
    depends_on("py-setuptools-scm@3.4.1: +toml", type="build")
