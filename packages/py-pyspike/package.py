# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyPyspike(PythonPackage):
    """
    PySpike is a Python library for the numerical analysis of spike train similarity.
    """

    homepage = "https://github.com/mariomulansky/PySpike"
    url = 'https://github.com/mariomulansky/PySpike/archive/refs/tags/0.8.0.tar.gz'

    maintainers = ['dionperd', 'paulapopa', "ldomide"]

    version('0.8.0', '199d41af097e0b6e6583e22d4a9c3cedab51ceba4da2d940682ffefe8120a414')
    version('0.7.0', '47031ba10a5726845982b62dcae970449ca50c4be9985a1ed0d2a021456bf25a')

    patch("cython3.patch", when="^py-cython@3:")

    # python_requires
    depends_on('python@3.8:', type=('build', 'run'))

    # setup_requires
    depends_on('py-pip', type='build')
    depends_on('py-setuptools', type=('build'))

    # install_requires
    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-matplotlib', type=('build', 'run'))
    depends_on('py-pytest', type=('build', 'run'))
    depends_on('py-cython', type=('build', 'run'))

    # Test dependency
    depends_on('py-pytest', type='test')
    
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which('pytest')
        pytest()
