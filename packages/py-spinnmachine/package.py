# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PySpinnmachine(PythonPackage):
    """This package is used to provide a Python representation of a SpiNNaker
    machine."""

    homepage = "https://github.com/SpiNNakerManchester/SpiNNMachine"
    pypi = "SpiNNMachine/SpiNNMachine-1!7.0.0.tar.gz"

    version("7.0.0", sha256="5da374fd9208287799fbc324136fe5954dd1b370792ea81ea10d4537643272ad")

    depends_on("python@3.7:", type=("build", "run"))
    depends_on("py-spinnutilities@7.0.0", type=("build", "run"))
