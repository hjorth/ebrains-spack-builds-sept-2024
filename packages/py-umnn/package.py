# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyUmnn(PythonPackage):
    """Official implementation of Unconstrained Monotonic Neural Networks (UMNN)."""

    homepage = "https://github.com/AWehenkel/UMNN"
    pypi = "umnn/umnn-1.71.tar.gz"

    version("1.71", "bdd41d941a5d904e2217a960a9584922afad8068304976dc6fb0245e4f834996")

    depends_on("python@3.6:", type=("build", "run"))
    depends_on("py-hatchling", type="build")
    depends_on("py-numpy", type=("build", "run"))
    depends_on("py-torch@1.1:", type=("build", "run"))
