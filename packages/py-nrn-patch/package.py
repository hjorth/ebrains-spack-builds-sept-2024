# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyNrnPatch(PythonPackage):
    """A patch to make the BSB interface with NEURON software"""

    homepage = "https://github.com/dbbs-lab/patch"
    pypi = "nrn-patch/nrn_patch-4.0.0.tar.gz"

    license("GPL-3.0-only")
    maintainers = ["helveg","filimarc","drodarie"]

    version("4.0.0", sha256="0f95243798c7363826d7835023f7c9215577edd8d6695cc7caeb65a7fe8a54c0")

    depends_on("py-flit-core@3.2:4.0", type="build")
    depends_on("py-numpy@1.21:")
    depends_on("py-errr@1.2:")
    depends_on("py-click@8.0:")
    depends_on("py-appdirs@1.0:")
    depends_on("py-cookiecutter@2.0:")
    depends_on("py-black@0.24:")
    depends_on("py-toml@0.1:")
    depends_on("py-nmodl-glia@4.0:")
    depends_on("neuron@8:10")
