# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class Sda(MakefilePackage):
    """
    SDA 7 can be used to carry out Brownian dynamics simulations of the diffusional association
    in a continuum aqueous solvent of two solute molecules, e.g. proteins, or of a solute
    molecule to an inorganic surface. SDA 7 can also be used to simulate the diffusion of
    multiple proteins, in dilute or concentrated solutions, e.g., to study the effects of
    macromolecular crowding.
    """

    # Url for the package's homepage.
    homepage = "https://mcm.h-its.org/sda/doc/doc_sda7/doc/doc_sda7/index.html"
    url      = "https://mcm.h-its.org/wiki/lib/exe/fetch.php?media=googlecolab:sda-7.3.3d.tgz"

    # A list of GitHub accounts to notify when the package is updated.
    maintainers = ['richtesn', 'thielblz']

    # Versions and checksums.
    version('7.3.3d', sha256='295ca0142532759a43375422a4ebc050e15b1c248e45493f5864765a9fe8076f')

    # Dependencies.
    depends_on('apbs', type=('build','run'))
    depends_on('python@3.8:', type=('build','run'))
    conflicts('%gcc@:9.3', when='@7.3.3d')
    conflicts('%intel')

    patch('sda-7.3.3d.patch', when='@7.3.3d')

    build_directory = 'src'
    build_jobs = 1

    def edit(self, spec, prefix):
        makefile = FileFilter('src/Makefile')
        makefile.filter(r'gfortran', spack_fc)
        makefile.filter(r'^\s*FC\s*=.*',  'FC = '  + spack_fc)
        makefile.filter(r'^\s*CC_plus\s*=.*', 'CXX = ' + spack_cxx)
        makefile.filter(r'\${CC_plus}', '${CXX}')
        makefile.filter(r'^\s*CC_moins\s*=.*', 'CC = ' + spack_cc)
        makefile.filter(r'\${CC_moins}', '${CC}')
    
    def install(self, spec, prefix):
        install_tree('auxi', prefix.auxi)
        install_tree('bin', prefix.bin)
        install_tree('examples', prefix.examples)
        install_tree('lib', prefix.lib)

    @run_before('build')
    def make_clean(self):
        with working_dir('src'):
            make("clean")

    @run_after('install')
    def test_install(self):
        # run random test
        with working_dir(self.prefix.examples + '/bnbs/bnbs_assoc_hits'):
            sda_flex = Executable(self.prefix.bin + '/sda_flex')
            sda_flex('sda_association_0.in')
