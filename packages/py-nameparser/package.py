# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyNameparser(PythonPackage):
    """A simple Python module for parsing human names into their individual components
    """

    homepage = "https://github.com/derek73/python-nameparser"
    pypi     = "nameparser/nameparser-1.1.1.tar.gz"
    git      = "https://github.com/derek73/python-nameparser.git"

    version('1.1.1', sha256='ce8336ed7464fa7b9b8745f48b9c62dea6bed931b99715ca9479360544992143')

    depends_on('python@3.6.9:')
    depends_on('py-setuptools', type=('build'))
