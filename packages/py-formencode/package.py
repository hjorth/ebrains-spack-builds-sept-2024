# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *

class PyFormencode(PythonPackage):
    """
    Requirement necessary for py-tvb-framework package.
    """

    homepage = "https://pypi.org/project/FormEncode"
    pypi = 'FormEncode/FormEncode-2.1.0.tar.gz'

    maintainers = ['paulapopa', 'ldomide']

    version('2.1.0', 'eb74d223078a28cf015fa88966c6e34f2d18d75127318d65c144bed9afc4263f')
    version('2.0.1', '8f2974112c2557839d5bae8b76490104c03830785d923abbdef148bf3f710035')
    version('2.0.0', 'f2eb92297417eb64e4aa8e368783a5ac1311e385d4f3ff3a181090608ea83711')

    # python_requires
    depends_on('python@3.8:', type=('build', 'run'))

    # setup_requires
    depends_on('py-pip', type=('build'))
    depends_on('py-setuptools', type=('build', 'run'))
    depends_on('py-six', type='run')
