# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyZuko(PythonPackage):
    """Python package that implements normalizing flows in PyTorch."""

    homepage = "https://github.com/probabilists/zuko"
    pypi = "zuko/zuko-1.3.1.tar.gz"

    version("1.3.1", "00f246802d3f486183185529ba22e0b2bf691397e03b28150a5cf713fa0da758")

    depends_on("python@3.9:", type=("build", "run"))
    depends_on("py-setuptools", type="build")
    depends_on("py-numpy@1.20.0:", type=("build", "run"))
    depends_on("py-torch@1.12.0:", type=("build", "run"))
