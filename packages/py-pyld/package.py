# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyPyld(PythonPackage):
    """JSON-LD processor written in Python."""

    homepage = "https://github.com/digitalbazaar/pyld"
    pypi = "PyLD/PyLD-0.8.2.tar.gz"

    version("0.8.2", sha256="f11b8586d2d2bc310739a9c49018574d01b8adc4533e950f64c85a13909d7630")

    depends_on("py-setuptools", type="build")

    depends_on("py-requests", type=("build", "run"))
