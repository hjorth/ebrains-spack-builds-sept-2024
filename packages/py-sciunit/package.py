# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PySciunit(PythonPackage):
    """A test-driven framework for formally validating scientific models against data
    """

    homepage = "http://sciunit.scidash.org/"
    pypi     = "sciunit/sciunit-0.2.5.1.tar.gz"
    git      = "https://github.com/scidash/sciunit.git"

    version('0.2.8', sha256='85b7288200e55a3270d2346cc357c19d2c812140a9398eda52152a6cb5a281f5')
    version('0.2.5.1', sha256='6148704f92a29c9d6de65ca9455b03ebe1f05101dae5e706aee2186e5a09fab3')

    depends_on('python@3.6.9:')
    depends_on('py-setuptools',                     type=('build'))
    depends_on('py-beautifulsoup4',                 type=('build', 'run'))
    depends_on('py-cerberus@1.3.4',                 type=('build', 'run'))
    depends_on('py-deepdiff',                       type=('build', 'run'))
    depends_on('py-gitpython',                      type=('build', 'run'))
    depends_on('py-importlib-metadata',             type=('build', 'run'))
    depends_on('py-ipykernel',                      type=('build', 'run'))
    depends_on('py-ipython',                        type=('build', 'run'))
    depends_on('py-jsonpickle',                     type=('build', 'run'))
    depends_on('py-lxml',                           type=('build', 'run'))
    depends_on('py-matplotlib',                     type=('build', 'run'))
    depends_on('py-nbconvert',                      type=('build', 'run'))
    depends_on('py-nbformat',                       type=('build', 'run'))
    depends_on('py-pandas@0.18:',                   type=('build', 'run'))
    depends_on('py-quantities-scidash@0.12.4.3:',   type=('build', 'run'))  
