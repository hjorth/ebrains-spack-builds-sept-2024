# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyNmodl(PythonPackage):
    """The NMODL Framework is a code generation engine for NEURON MODeling Language (NMODL)."""

    homepage = "https://github.com/BlueBrain/nmodl"

    git = "https://github.com/BlueBrain/nmodl"

    license("Apache-2.0")
    maintainers = ["bbp.opensource"]

    version("0.5", tag="0.5", commit="ac272785dc444c8444b085d121f08b7575bb6647", submodules=True)

    patch("fix-setup-requirements.patch", when="@:0.6")

    depends_on("flex@2.6:")
    depends_on("bison@3.0:")
    depends_on("cmake@3.15:", type="build")
    depends_on("python@3.9:", type=("build","run"))
    depends_on("py-setuptools", type="build")
    depends_on("py-scikit-build", type="build")
    depends_on("py-jinja2@2.9.3:", type="build")
    depends_on("py-pyyaml@3.13:", type="build")
    depends_on("py-pytest")
    depends_on("py-sympy@1.3:", type=("build","run"))
    depends_on("py-find-libpython", type=("build","run"))
    depends_on("py-importlib-metadata", when="^python@:3.8", type=("build","run"))
    depends_on("py-importlib-resources", when="^python@:3.8", type=("build","run"))

    def setup_build_environment(self, env):
        env.set("NMODL_WHEEL_VERSION", self.version)
