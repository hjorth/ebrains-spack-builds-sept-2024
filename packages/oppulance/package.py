# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *
import unittest.mock


class Oppulance(Package):
    """SDK for embedded processors on BrainScaleS-2"""

    homepage = "https://github.com/electronicvisions/oppulance"

    # PPU compiler dependencies
    depends_on('gettext')
    depends_on('zlib')
    depends_on('bison')
    depends_on('flex@2.6.4:')
    depends_on('m4')
    depends_on('texinfo')
    depends_on('wget')
    depends_on('gmp')

    version(
        "10.0-a1",
        git="https://github.com/electronicvisions/releases-ebrains",
        tag="ebrains-10.0-a1",
        commit="d9bd675b446be8f313972aef2d6657ffbbb91ed2",
        submodules=True,
    )
    version(
        "9.0-a9",
        git="https://github.com/electronicvisions/releases-ebrains",
        tag="ebrains-9.0-a9",
        commit="41d2597bd6c1c20aee4d538c42c248195a133680",
        submodules=True,
    )
    version(
        "9.0-a8",
        git="https://github.com/electronicvisions/releases-ebrains",
        tag="ebrains-9.0-a8",
        commit="44323be431da4b4b43890815f453c27207dee0b2",
        submodules=True,
    )
    version(
        "9.0-a7",
        git="https://github.com/electronicvisions/releases-ebrains",
        tag="ebrains-9.0-a7",
        commit="2337adc6a33f907900d2b8be5d9f0b15872a200a",
        submodules=True,
    )
    version(
        "9.0-a6",
        git="https://github.com/electronicvisions/releases-ebrains",
        tag="ebrains-9.0-a6",
        commit="e9b6746edb5e8465ae2848556b70e4edd555182e",
        submodules=True,
    )
    version(
        "9.0-a5",
        git="https://github.com/electronicvisions/releases-ebrains",
        tag="ebrains-9.0-a5",
        commit="5fcb0682626d83c089e016aaf433e0938e921634",
        submodules=True,
    )
    version(
        "9.0-a4",
        git="https://github.com/electronicvisions/releases-ebrains",
        tag="ebrains-9.0-a4",
        commit="8d5be2a23ac1eb0129bdf1f83fb0a1c5193c4c85",
        submodules=True,
    )

    releases = [
        {
            'version': '8.0-a5',
            'tag': 'ebrains-8.0-a5'
        },
        {
            'version': '8.0-a4',
            'tag': 'ebrains-8.0-a4'
        },
        {
            'version': '8.0-a3',
            'tag': 'ebrains-8.0-a3'
        },
        {
            'version': '8.0-a2',
            'tag': 'ebrains-8.0-a2'
        },
        {
            'version': '8.0-a1',
            'tag': 'ebrains-8.0-a1'
        },
        {
            'version': '7.0-rc1-fixup3',
            'tag': 'ebrains-7.0-rc1-fixup3'
        },
        {
            'version': '7.0-rc1-fixup2',
            'tag': 'ebrains-7.0-rc1-fixup2'
        },
        {
            'version': '7.0-rc1-fixup1',
            'tag': 'ebrains-7.0-rc1-fixup1'
        },
    ]

    for release in releases:
        version(
            release['version'],
            git='https://github.com/electronicvisions/oppulance',
            tag=release['tag'],
        )

        for res in ['binutils-gdb', 'gcc', 'newlib']:
            resource(
                name=res,
                git='https://github.com/electronicvisions/{}'.format(res),
                tag=release['tag'],
                placement=res,
                when="@"+release['version'],
            )

    # defined by gcc/contrib/download_prerequisites
    resource(name='gmp-6.1.0.tar.bz2',
             url='http://gcc.gnu.org/pub/gcc/infrastructure/gmp-6.1.0.tar.bz2',
             sha256='498449a994efeba527885c10405993427995d3f86b8768d8cdf8d9dd7c6b73e8',
             expand=False,
             )
    resource(name='mpfr-3.1.4.tar.bz2',
             url='http://gcc.gnu.org/pub/gcc/infrastructure/mpfr-3.1.4.tar.bz2',
             sha256='d3103a80cdad2407ed581f3618c4bed04e0c92d1cf771a65ead662cc397f7775',
             expand=False,
             )
    resource(name='mpc-1.0.3.tar.bz2',
             url='http://gcc.gnu.org/pub/gcc/infrastructure/mpc-1.0.3.tar.gz',
             sha256='617decc6ea09889fb08ede330917a00b16809b8db88c29c31bfbb49cbf88ecc3',
             expand=False,
             )
    resource(name='isl-0.18.tar.bz2',
             url='http://gcc.gnu.org/pub/gcc/infrastructure/isl-0.18.tar.bz2',
             sha256='6b8b0fd7f81d0a957beb3679c81bbb34ccc7568d5682844d8924424a0dadcb1b',
             expand=False,
             )

    # see build-brainscales package for a description; we need to duplicate
    # here, as the cache content is shared between the two repositories
    @when("@9:")
    def do_fetch(self, mirror_only=False):
        # in the configure step, we need access to all archived .git folders
        def custom_archive(self, destination):
            super(spack.fetch_strategy.GitFetchStrategy, self).archive(destination)
        with unittest.mock.patch('spack.fetch_strategy.GitFetchStrategy.archive', new=custom_archive):
            super().do_fetch(mirror_only)

    def install(self, spec, prefix):
        ln = which('ln')
        # move gcc resources into place
        for key in self.resources:
            for res in self.resources[key]:
                if not res.name.startswith(
                        ('gmp-', 'mpfr-', 'mpc-', 'isl-')):
                    continue
                assert(res.fetcher.stage.archive_file)
                ln('-sf', res.fetcher.stage.archive_file, 'gcc/')
        bash = which('bash')
        # extracts the gcc resources via gcc/contrib/download_prerequisites
        # (download is skipped if file exists)
        bash('gcc/ci/00_download_prerequisites.sh')
        bash('binutils-gdb/ci/00_build_install.sh')
        bash('gcc/ci/01_build_install_freestanding.sh')
        bash('newlib/ci/00_build_install.sh')
        wdir = spec.version >= Version("9") and "oppulance" or "."
        bash(wdir + '/ci/00_build_install_libstdc++.sh')
        mkdirp(spec.prefix)
        install_tree('install/.', spec.prefix)

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install(self):
        ppu_gcc = which('powerpc-ppu-gcc')
        ppu_gcc('--version')
