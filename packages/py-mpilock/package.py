# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyMpilock(PythonPackage):
    """mpilock offers a WindowController class with a high-level API for parallel access to resources. """

    homepage = "https://github.com/Helveg/mpilock"
    pypi = "mpilock/mpilock-1.1.0-py3-none-any.whl"

    version("1.1.0", sha256="0902ef859a7b3dfb4312a3c46332302493aa14fa398b610554706b0b9e7cb57c", expand=False)

    maintainers=["helveg"]

    depends_on("py-setuptools", type="build")
    depends_on("py-mpi4py@3.0.3:")
    depends_on("py-numpy@1.20.0:")
