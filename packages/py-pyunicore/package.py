# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyPyunicore(PythonPackage):
    """Python library for using the UNICORE REST API"""

    homepage = "https://github.com/HumanBrainProject/pyunicore"
    pypi     = "pyunicore/pyunicore-0.15.0.tar.gz"

    maintainers = ['bschulle']

    version('1.1.1', sha256='6be3b8157bba1012d31da3a5c1236f35a034f5ab656eefb045943fa72ebec795')
    version('1.0.1', sha256='822cc372b2fa24fdd4626409f4a5f93cc77d2e30dbf1bc9e198e0b2c5b194a57')
    version('1.0.0', sha256='dff3f6d78a66a90fdd95db3009fb0b6e1d39a3686687f4aeac5b4b1234b93783')
    version('0.15.0', sha256='dc57ef05b1681b20471e8a4a72067671f74b4d41601099fe80324293439175d5')
    version('0.14.1', sha256='7efd4784d55ef02c4da4431232b54505814666577165c0f320c922ed2b32af01')
    version('0.14.0', sha256='8197aea4009ceb150bc4e341f660c942e424dca5025bc0253c8729328835a0c0')
    version('0.12.0', sha256='a1d7a63528cb0065d5ad58a42bc00adcf48614e16c2337dd815c8ac4d81b60cb')
    version('0.11.4', sha256='519b23a56103c1d8dd7d9a76029785dd89aa424de8bf9df345baf591f816553c')
    version('0.11.3', sha256='bb5582107daff9ddb6444f35eff019791009f04927508c4c9ae918f22effb718')

    variant("fuse",   default=False, description="Use the UFTP fuse driver")
    variant("fs",     default=False, description="Use UFTP with pyfilesystem")
    variant("crypto", default=False, description="Create JWT tokens signed with keys")

    depends_on('py-setuptools',          type='build')
    depends_on('py-pyjwt@2.0:',          type=('build', 'run'))
    depends_on('py-requests@2.5:',       type=('build', 'run'))
    depends_on('py-fusepy@3.0.1:',       type=('build', 'run'), when='+fuse')
    depends_on('py-fs@2.4.0:',           type=('build', 'run'), when='+fs')
    depends_on('py-cryptography@3.3.1:', type=('build', 'run'), when='+crypto')
    depends_on('py-bcrypt@4.0.0:',       type=('build', 'run'), when='@1.0.1:+crypto')
    depends_on('py-pytest',              type=('test'))
