# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyTvbContrib(PythonPackage):
    """
    Various auxiliary scripts used with TVB.
    """

    homepage = "https://www.thevirtualbrain.org/"
    pypi = 'tvb-contrib/tvb_contrib-2.9.1.tar.gz'

    maintainers = ['dionperd', 'paulapopa', "ldomide"]

    version('2.9.1', '23c8cc11737f8b87d8e8db573f9f3701611ccc1b6ca67fac00cd09e6307cae6c')
    version('2.9', '64e5f17c46b67c1f92b3cdfb863c7d5e1d97d913b26521a9567735270bb9ad25')
    version('2.8.2', 'ff5990e979cf1f87046d7bed2c265f9dec50f9d817abaabdf5df7ec4482b5063')
    version('2.8.1', '0b9bc5f837913f1cba007d25ae94c9df51bf43bf19c9fb2af14a90cb6a1e1a66')

    # python_requires
    depends_on('python@3.8:', type=('build', 'run'))

    # setup_requires
    depends_on('py-pip', type='build')
    depends_on('py-setuptools', type='build')

    # install_requires
    depends_on('py-tvb-library', type=('build', 'run'))
    depends_on('py-xarray', type=('build', 'run'))
    depends_on('py-scikit-learn', type=('build', 'run'))

    # py-dill:
    # https://github.com/spack/spack/blob/develop/var/spack/repos/builtin/packages/py-dill/package.py
    depends_on('py-dill', type=('build', 'run'))

    # Test dependency
    depends_on('py-pytest', type='test')
    
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
       pytest = which('pytest')
       pytest()
    
