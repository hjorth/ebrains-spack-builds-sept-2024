# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class Log4cxx(CMakePackage):
    """A C++ port of Log4j"""

    homepage = "https://logging.apache.org/log4cxx/latest_stable/"
    # begin EBRAINS (modified): fix url
    url = "https://github.com/apache/logging-log4cxx/archive/refs/tags/rel/v1.2.0.tar.gz"
    # end EBRAINS

    maintainers("nicmcd")

    license("Apache-2.0", checked_by="wdconinc")

    # begin EBRAINS (modified): fix checksums
    version("1.2.0",  sha256="3e0af426011718c634194200cdd79b49ec13c322697bdcddef3d8b2ac9efd7b6")
    with default_args(deprecated=True):
        # https://nvd.nist.gov/vuln/detail/CVE-2023-31038
        version(
            "0.12.1", sha256="567a4200c5b005a816c401e798d98294782950c7750eb3e285e851b970c8beed"
        )
        version(
            "0.12.0", sha256="31730a17b8ff3f416256755b7aa6d7e95b167c670eb469eb9ff99aa006376e79"
        )
    # end EBRAINS

    variant(
        "cxxstd",
        default="17",
        description="C++ standard",
        values=("11", "17"),
        multi=False,
        when="@:1.1",
    )
    variant(
        "cxxstd",
        default="20",
        description="C++ standard",
        values=("11", "17", "20"),
        multi=False,
        when="@1.2:",
    )
    # begin EBRAINS (added)
    variant("events_at_exit",
            default=False,
            description="Enable to use logging during the application termination"
            )
    # end EBRAINS

    depends_on("cmake@3.13:", type="build")

    depends_on("apr-util")
    depends_on("apr")
    depends_on("boost+thread+system", when="cxxstd=11")
    depends_on("expat")
    depends_on("zlib-api")
    depends_on("zip")

    def cmake_args(self):
        return [
            self.define_from_variant("CMAKE_CXX_STANDARD", "cxxstd"),
            self.define("BUILD_TESTING", "off"),
            # begin EBRAINS (added)
            self.define_from_variant("LOG4CXX_EVENTS_AT_EXIT", "events_at_exit"),
            # end EBRAINS
        ]
