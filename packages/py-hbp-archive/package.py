# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyHbpArchive(PythonPackage):
    """A high-level API for interacting with the Human Brain Project
        archival storage at CSCS
    """

    homepage = "https://hbp-archive.readthedocs.io/"
    pypi     = "hbp-archive/hbp_archive-1.1.1.tar.gz"
    git      = "https://github.com/HumanBrainProject/hbp_archive.git"

    version('1.1.1', sha256='08059d3bcca4dcbc36951c82e75513bc50d8d59f1720a28bebf57a622d869f41')

    depends_on('python@3.6.9:')
    depends_on('py-setuptools',                   type=('build'))
    depends_on('py-lxml@4.6.4:',                  type=('build', 'run'))
    depends_on('py-keystoneauth1@4.3.1:',         type=('build', 'run'))
    depends_on('py-python-keystoneclient@4.2.0:', type=('build', 'run'))
    depends_on('py-python-swiftclient@3.12.0:',   type=('build', 'run'))
