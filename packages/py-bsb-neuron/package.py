# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyBsbNeuron(PythonPackage):
    """
    The BSB-NEURON is a component framework for neural modeling, used for simulate with NEURON software.
    """

    homepage = "https://github.com/dbbs-lab/bsb-neuron"
    pypi = "bsb-neuron/bsb_neuron-4.2.2.tar.gz"

    license("GPL-3.0-only")
    maintainers = ["helveg","filimarc","drodarie"]

    version("4.2.2", sha256="e7570c0cb17d31349eb8e88487e8ba48653f0fad0d7c232df8815cadde34a941")

    depends_on("py-flit-core@3.2:4.0", type="build")
    depends_on("py-bsb-core@5.0.2:")
    depends_on("neuron")
    depends_on("py-arborize@4.1:")
    depends_on("py-nrn-patch@4:")
