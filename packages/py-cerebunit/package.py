# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyCerebunit(PythonPackage):
    """CerebUnit is a component for validating cerebellum models that contains the validation tests.
    It is one of the four components for validating cerebellum models:
    CerebModels and CerebData do not require installation while CerebUnit and CerebStats require installation."""

    homepage = "https://cerebunit.readthedocs.io"
    pypi     = "cerebunit/cerebunit-0.0.1.tar.gz"
    git = "https://github.com/cerebunit/cerebunit.git"

    maintainers = ['lungsi', 'apdavison'] # github usernames

    version('0.0.1', sha256='f5b3811c5da2e8416a30a67497b90c87cba176493662e73f2191f1375a6d7bfe')

    depends_on('py-cerebstats', type=('build', 'run'))
    depends_on('py-numpy', type=('build', 'run'))
    depends_on('py-scipy', type=('build', 'run'))
    depends_on('py-quantities', type=('build', 'run'))
    depends_on('py-sciunit', type=('build', 'run'))
    depends_on('py-pynwb@1.0.2', type=('build', 'run'))
    depends_on('py-hdmf@1.0.3', type=('build', 'run'))
    depends_on('py-h5py', type=('build', 'run'))
    depends_on('py-pandas', type=('build', 'run'))

