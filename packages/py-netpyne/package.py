# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyNetpyne(PythonPackage):
    """NetPyNE (Networks using Python and NEURON) is a Python package to facilitate the development, simulation, parallelization, analysis, and optimization of biological neuronal networks using the NEURON simulator."""

    homepage = "netpyne.org"
    url = "https://github.com/suny-downstate-medical-center/netpyne/archive/refs/tags/v1.0.5.tar.gz"
    git = "https://github.com/suny-downstate-medical-center/netpyne.git"

    # list of GitHub accounts to notify when the package is updated.
    maintainers = ["salvadord", "vvbragin"]

    version("1.0.6",   sha256="7c23cdd6a4de333ab9931ae11e86f61e2b21956034c23bcdf7a42d18ec9a3a83")
    version("1.0.5",   sha256="482ceceef87afd587c5df5ae62e31993fcb2f68c9f09fe2b1548a94e7797a3e3")
    version("1.0.4.2", sha256="4191464386bbc2ecad3b224302c19edb9a26fe0da57c0d9b234cb9c2dd918c8d")
    version("1.0.4.1", sha256="2ed12807f9b3d3e3a8ba9910f8d22c5c17635e0cdea0af09fa3a230e68876df6")

    depends_on("py-setuptools", type="build")

    depends_on("neuron", type=("build", "run"))
    depends_on("py-numpy", type=("build", "run"))
    depends_on("py-scipy", type=("build", "run"))
    depends_on("py-matplotlib", type=("build", "run"))
    depends_on("py-matplotlib@:3.5.1", type=("build", "run"), when="@:1.0.4.1")
    depends_on("py-matplotlib-scalebar", type=("build", "run"))
    depends_on("py-future", type=("build", "run"))
    depends_on("py-pandas", type=("build", "run"))
    depends_on("py-bokeh", type=("build", "run"))
    depends_on("py-schema", type=("build", "run"))
    depends_on("py-lfpykit", type=("build", "run"))
    depends_on('py-pytest', type='test')

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which('pytest')
        pytest('tests/doc/test_tutorials.py')
