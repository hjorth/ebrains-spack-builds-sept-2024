# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PySiibra(PythonPackage):
    """Software interfaces for interacting with brain atlases"""

    homepage = "https://github.com/FZJ-INM1-BDA/siibra-python"
    pypi     = "siibra/siibra-0.4a57.tar.gz"

    maintainers = ['x.gui@fz-juelich.de', 's.koehnen@fz-juelich.de', 't.dickscheid@fz-juelich.de']

    version("1.0a9", sha256="d495738d7d618d4f4da8b0747ef26b6db50b4bde223ba7d9d7f599ca87e37e90")
    version("0.5a2", sha256="6f40eeebf9225da18772658c69d0fa1fcff9b93a684f0863709b369adca38927")
    version("0.4a78", sha256="39dd631c1231d0422cea1783b727811ddf8210316cb84f035b4bc4299e161f33")
    version("0.4a66", sha256="75bd925159d2c4bc904b97824ab8d48a4162f07c11ae0ee4d358427bd3badccb")
    version("0.4a64", sha256="651f3382c7e3407261ca71921614e3bf64f3205c1b7072ce0bb482d65fa73be3")
    version("0.4a63", sha256="225c2cbf98ec57bee4063176f868a2a10ab153ac917e162843d8a26a38ba78b4")
    version("0.4a62", sha256="4ff6f36e7bd12878d71c148cdf50ac3434c50248abe61d610cb01adad129eb52")
    version("0.4a61", sha256="9ebb3402a4789935c3f75e7a812d8d2500c183187d4ae99cac9b554f4dbe84f3")
    version("0.4a59", sha256="e878b424dfece6c379146674c2fe0875ec290811aa6a8323ef1404d25c0a9010")
    version("0.4a58", sha256="f24660f094932628f088ef3f9575b196fd360bd74a1e1c8e2627d985ed35e210")
    version("0.4a57", sha256="53983c4baab84abe5dd6928c320ce87433cab02b5ed992995f0a17661c93dbff")
    version("0.4a56", sha256="4c03d8a94c73a233e216103fa7090d057db03b1806027fabde38e8f83b852d78")
    version("0.4a54", sha256="114765afe906383c7e6b930f1df7e26ddc15a6f295e2eab83a861eaba6d8f57f")
    version("0.4a51", sha256="5aacd53b9b59a98a061c6b95421ab588d35ea7f6f27753c634a42ea7324cf9fe")
    version("0.4a47", sha256="661b68cd82d4ffbf3f815fb961543cc8c42b695ee601d5856ce90bfc7ccb968a")
    version("0.4a46", sha256="ddf17cd25c8249111d9f8c0c1088fabcc0fa8796734dc1784506155df6d57568")
    version("0.4a35", sha256="106a61824c6e3260ee65241add88bbb2d6be4a48756c1e3173860406722a0dc0")
    version("0.4a33", sha256="d67ba51547cb7fbc792bd14e93d083cabeac978b9a3ee4bfd5cf9aa0c95c5c94")
    version("0.4a32", sha256="bfeca6c56cc33630a8e046f0ba8a4d9e1168916e64ffe730b4ed08b8ad06bb4c")
    version("0.4a31", sha256="e49a1583b0b60403d799bca031c05208acd57bf5d3a605cefb3e9439bc36410d")
    
    depends_on('python@3.7:',             type=('build', 'run'))

    depends_on('py-anytree',              type=('build', 'run'))
    depends_on('py-nibabel',              type=('build', 'run'))
    depends_on('py-appdirs',              type=('build', 'run'))
    depends_on('py-scikit-image',         type=('build', 'run'))
    depends_on('py-requests',             type=('build', 'run'))
    depends_on('py-neuroglancer-scripts', type=('build', 'run'))
    depends_on('py-nilearn',              type=('build', 'run'))
    depends_on('py-typing-extensions',    type=('build', 'run'), when='^python@:3.7')
    depends_on('py-filelock',             type=('build', 'run'))
    depends_on('py-ebrains-drive',        type=('build', 'run'), when='@0.4a74:')

    depends_on('py-pytest',               type=('test'))

    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self): 
        pytest = which("pytest")
        pytest("-rx")

