# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class WfBsb(BundlePackage):
    """Meta-package to collect all dependencies for the BSB."""

    homepage="https://github.com/dbbs-lab/bsb"

    maintainers = ["helveg","filimarc","drodarie"]

    version("4.4")

    variant('nest', default=False,
            description='Build with NEST interface')
    variant('neuron', default=False,
            description='Build with NEURON interface')

    depends_on("py-bsb-core@5.0.2:")
    depends_on("py-bsb-hdf5@5.0.2:")
    depends_on("py-bsb-json@4.2.2:")
    depends_on("py-bsb-yaml@4.2.2:")

    depends_on("py-bsb-nest",when="+nest")
    depends_on("py-bsb-neuron",when="+neuron")
