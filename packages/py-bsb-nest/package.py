# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyBsbNest(PythonPackage):
    """
    The BSB-NEST is a component framework for neural modeling, used for simulate SNN with NEST software.
    """

    homepage = "https://github.com/dbbs-lab/bsb-nest"
    pypi = "bsb-nest/bsb_nest-4.3.2.tar.gz"

    license("GPL-3.0-only")
    maintainers = ["helveg","filimarc","drodarie"]

    version("4.3.2", sha256="478aa2937ca554ff291ce726cc69e1c1b283d7353a56e3b6878b585ed0684041")

    depends_on("py-flit-core@3.2:4.0", type="build")
    depends_on("py-bsb-core@5.0.2:")
    depends_on("nest")
