# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyDicthash(PythonPackage):
    """Generate portable md5 hashes from (arbitrarily nested) dictionaries.

    These dictionaries can contain arbitrary Python and NumPy data types. The
    goal of the module is to provide a hash function that can be safely used
    across different platforms. Its main use is to generate unique identifiers
    for parameter dictionaries used in parameter scans of neural network
    simulations.
    """

    homepage = "https://python-dicthash.readthedocs.io"
    pypi     = "dicthash/dicthash-0.0.2.tar.gz"
    git = "https://github.com/Happy-Algorithms-League/python-dicthash"

    maintainers = ['terhorstd', 'jakobj']

    version('0.0.2', sha256='30b71bd64101295053b082b3244870e6ca7dca561bdb8f3776c42c8c40cb4af4')

    depends_on('py-setuptools', type='build')

