# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *

class PyBiobbAnalysis(PythonPackage):
    """Biobb_analysis is the Biobb module collection to perform analysis 
    of molecular dynamics simulations"""

    # Homepage and download url
    homepage = "https://github.com/bioexcel/biobb_analysis"
    git = 'https://github.com/bioexcel/biobb_analysis.git'
    url = 'https://github.com/bioexcel/biobb_analysis/archive/refs/tags/v4.1.0.tar.gz'

    # Set the gitlab accounts of this package maintainers
    maintainers = ['dbeltran']

    # Versions
    version('4.1.0', sha256='fecbb7ffa0e38f732fcc613adc7f1656e3c65af519a072269dabc244681f1791')

    # Dependencies
    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('py-biobb-common')
    depends_on('gromacs')
    depends_on('ambertools')

    # Test
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install (self):
        python("-c", 'import biobb_analysis')
