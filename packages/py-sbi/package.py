# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PySbi(PythonPackage):
    """Python package for simulating biological systems."""

    homepage = "https://sbi-dev.github.io/sbi/v0.23.3/"
    url = "https://github.com/sbi-dev/sbi/archive/refs/tags/v0.23.3.tar.gz"

    version("0.23.3", "b1ef102e47c90088f2adfff5ea88b18421e84c4641ff4dd4f68c1116c296ba81")

    depends_on("python@3.9:", type=("build", "run"))
    depends_on("py-setuptools", type="build")
    depends_on("py-wheel", type="build")

    depends_on("py-arviz", type=("build", "run"))
    depends_on("py-joblib@1.3.0:", type=("build", "run"))
    depends_on("py-matplotlib", type=("build", "run"))
    depends_on("py-notebook@:6.4.12", type=("build", "run"))
    depends_on("py-numpy@:1", type=("build", "run"))
    depends_on("py-pillow", type=("build", "run"))
    depends_on("py-pyknos@0.16.0:", type=("build", "run"))
    depends_on("py-pyro-ppl@1.3.1:", type=("build", "run"))
    depends_on("py-scikit-learn", type=("build", "run"))
    depends_on("py-scipy", type=("build", "run"))
    depends_on("py-tensorboard", type=("build", "run"))
    depends_on("py-torch@1.13.0:", type=("build", "run"))
    depends_on("py-tqdm", type=("build", "run"))
    depends_on("py-pymc@5.0.0:", type=("build", "run"))
    depends_on("py-zuko@1.2.0:", type=("build", "run"))

    depends_on("py-pytest", type="test")
    depends_on("py-torchtestcase", type="test")

    skip_modules = ["sbi.inference.snle", "sbi.inference.snpe", "sbi.inference.snre", "sbi.samplers.score", "sbi.samplers.vi"]

    @run_after("install")
    @on_package_attributes(run_tests=True)
    def install_test(self):
        pytest = which("pytest")
        pytest("-m", "not slow and not gpu")
