# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyNnmt(PythonPackage):
    """NNMT is an open-source, community centered Python package for collecting 
    reusable implementations of analytical methods for neuronal network model analysis 
    based on mean-field theory."""

    homepage = "https://nnmt.readthedocs.io/en/latest/index.html"
    pypi = "nnmt/nnmt-1.3.0.tar.gz"

    maintainers = ["rshimoura", "terhorstd"]

    version("1.3.0", sha256="0cb4f7c58e08520e383506b5b15fb0a9552801adc03fd1006b9e3dd17b1b636d")

    depends_on("py-setuptools@23.1.0:", type="build")
    depends_on("py-numpy@1.8:", type=("build", "run"))
    depends_on("py-scipy@0.14:", type=("build", "run"))
    depends_on("py-cython@0.20:", type=("build", "run"))
    depends_on("py-h5py@2.5:", type=("build", "run"))
    depends_on("py-matplotlib@2.0:", type=("build", "run"))
    depends_on("py-pint", type=("build", "run"))
    depends_on("py-pyyaml", type=("build", "run"))
    depends_on("py-requests", type=("build", "run"))
    depends_on("py-mpmath", type=("build", "run"))
    depends_on("py-decorator", type=("build", "run"))
    depends_on("py-pytest@5.4:", type=("build", "run"))
    depends_on("py-pytest-mock@3.1:", type=("build", "run"))
    depends_on("python@3:", type=("build", "run"))
